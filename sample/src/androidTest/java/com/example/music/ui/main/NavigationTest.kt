package com.example.music.ui.main


import android.view.View
import android.view.ViewGroup
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.music.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class NavigationTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun navigationBackTest() {
        // Starts at "Start" fragment
        Thread.sleep(7000)

        // Goes to "Hotlist" bottom navigation menu
        val bottomNavigationItemView = onView(
                allOf(withId(R.id.searchFragment), withContentDescription("Hotlist"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavigationView),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())
        Thread.sleep(1000)

        // Click in a detail view in "Hotlist"
        val constraintLayout = onView(
                allOf(withId(R.id.loginLinearLayout),
                        childAtPosition(
                                allOf(withId(R.id.searchRecyclerView),
                                        childAtPosition(
                                                withClassName(`is`("android.widget.LinearLayout")),
                                                2)),
                                0),
                        isDisplayed()))
        constraintLayout.perform(click())

        // Goes back to "Hotlist" root
        pressBack()

        // Goes back to "Start"
        pressBack()

        // Goes again to "Hotlist"
        val bottomNavigationItemView2 = onView(
                allOf(withId(R.id.searchFragment), withContentDescription("Hotlist"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavigationView),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())

        // Click again in a detail view in "Hotlist"
        val constraintLayout2 = onView(
                allOf(withId(R.id.loginLinearLayout),
                        childAtPosition(
                                allOf(withId(R.id.searchRecyclerView),
                                        childAtPosition(
                                                withClassName(`is`("android.widget.LinearLayout")),
                                                2)),
                                0),
                        isDisplayed()))
        constraintLayout2.perform(click())

        // Goes back to "Hotlist" root
        pressBack()
        Thread.sleep(1000)

        // If everything went right, we should not see the detail post ImageView
        val imageView = onView(
                allOf(withId(R.id.postImageView))
        )
        imageView.check(doesNotExist())
        //imageView.check(ViewAssertions.matches(not(isDisplayed())))

        Thread.sleep(7000)
    }

    @Test
    fun navigationBasicFlowTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(7000)

        val constraintLayout = onView(
                allOf(withId(R.id.loginLinearLayout),
                        childAtPosition(
                                allOf(withId(R.id.sectionMediaRecyclerView),
                                        childAtPosition(
                                                withId(R.id.linearLayout),
                                                2)),
                                0),
                        isDisplayed()))
        constraintLayout.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(100)

        val bottomNavigationItemView = onView(
                allOf(withId(R.id.searchFragment), withContentDescription("Hotlist"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavigationView),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(100)

        val constraintLayout2 = onView(
                allOf(withId(R.id.loginLinearLayout),
                        childAtPosition(
                                allOf(withId(R.id.searchRecyclerView),
                                        childAtPosition(
                                                withClassName(`is`("android.widget.LinearLayout")),
                                                2)),
                                0),
                        isDisplayed()))
        constraintLayout2.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(100)

        val bottomNavigationItemView2 = onView(
                allOf(withId(R.id.favoritesFragment), withContentDescription("Library"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavigationView),
                                        0),
                                2),
                        isDisplayed()))
        bottomNavigationItemView2.perform(click())

        val constraintLayout3 = onView(
                allOf(withId(R.id.loginLinearLayout),
                        childAtPosition(
                                allOf(withId(R.id.sectionMediaRecyclerView),
                                        childAtPosition(
                                                withId(R.id.linearLayout),
                                                2)),
                                0),
                        isDisplayed()))
        constraintLayout3.perform(click())

        pressBack()

        pressBack()

        pressBack()

        pressBack()

        pressBack()

        val textView = onView(
                allOf(withId(R.id.sectionTitleTextView), withText("Clips recommandés"),
                        childAtPosition(
                                allOf(withId(R.id.linearLayout),
                                        childAtPosition(
                                                withId(R.id.startRecyclerView),
                                                0)),
                                1),
                        isDisplayed()))
        textView.check(ViewAssertions.matches(withText("Clips recommandés")))

        val viewGroup = onView(
                allOf(withId(R.id.playerToolbar),
                        childAtPosition(
                                allOf(withId(R.id.mediaFrameLayout),
                                        childAtPosition(
                                                IsInstanceOf.instanceOf(android.view.ViewGroup::class.java),
                                                0)),
                                1),
                        isDisplayed()))
        viewGroup.check(ViewAssertions.matches(isDisplayed()))
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
