package com.example.music.service

import com.example.music.data.Database
import com.example.music.data.Media
import com.example.music.data.Media.Companion.TYPE_MUSIC
import com.example.music.data.Media.Companion.TYPE_VIDEO
import com.example.music.data.Playlist
import com.example.music.data.User
import java.util.*


object MockSyncService {

    const val loremIpsum =
        "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi dignissim eget libero vitae hendrerit. Ut diam sem, euismod at enim quis, euismod fringilla risus. Proin non nisi vel leo ornare ullamcorper eu nec eros. Donec feugiat nulla vitae ante finibus molestie sed vitae lorem. Fusce id blandit nisi. Etiam quis ex vel tellus finibus pulvinar. Praesent in turpis non purus rutrum vulputate. Vestibulum faucibus at risus non luctus. Cras vehicula sem turpis. Etiam facilisis dapibus aliquet. Phasellus ultricies ex at purus hendrerit, non maximus nisi tristique.</p><p>Curabitur dapibus iaculis mauris non consequat. Etiam luctus ac dolor non tincidunt. In eu porta tellus, et finibus lectus. Nullam nec tortor turpis. Morbi dignissim quam sed turpis pretium, a egestas lorem dignissim. Nulla laoreet est nunc, eu viverra augue varius eu. Praesent fermentum est quis interdum tincidunt. Fusce sed sapien risus. Etiam pellentesque placerat diam ut ultrices. Proin laoreet nisi vitae nisl volutpat, sit amet posuere sem imperdiet. Nulla facilisi. Donec pretium, augue ut interdum sagittis, lacus nulla hendrerit nisi, ut pretium purus nisl rhoncus nisl. Maecenas lorem libero, ullamcorper nec elit a, imperdiet fringilla augue. Mauris pellentesque diam ut nisi varius, eu porttitor diam viverra. Nullam vulputate mi nec ullamcorper feugiat.</p>"

    val users = arrayOf(
        User(
            0,
            "Jon",
            "Jon Snow",
            Date(),
            "jon.snow@gmail.com",
            "https://instagram.fjdf3-1.fna.fbcdn.net/vp/1be53d32095e9cc367ee7b42b255f5b3/5CFEA877/t51.2885-19/s150x150/15802996_1896981293870528_4319071142091423744_a.jpg?_nc_ht=instagram.fjdf3-1.fna.fbcdn.net",
            "123456",
            10,
            0,
            "active"
        ),
        User(
            1,
            "MØ",
            "Karen Marie Aagaard Ørsted Andersen",
            Date(),
            "cersei@lannister.com",
            "https://upload.wikimedia.org/wikipedia/en/9/94/Cersei_Lannister-Lena_Headey.jpg",
            "123456",
            15,
            1,
            "active"
        ),
        User(
            2,
            "Bastille",
            "Dan Smith",
            Date(),
            "jaime@lannister.com",
            "https://pbs.twimg.com/profile_images/853288399293120514/kQU6rncY.jpg",
            "123456",
            15,
            2,
            "active"
        ),
        User(
            3,
            "Kungs",
            "Valentin Brunel",
            Date(),
            "daenerys@targaryen.com",
            "https://images.hellogiggles.com/uploads/2016/07/02042549/hbo-700x525.jpg",
            "123456",
            20,
            1,
            "active"
        ),
        User(
            4,
            "Lana del Rey",
            "Elizabeth Woolridge Grant",
            Date(),
            "daenerys@targaryen.com",
            "https://images.hellogiggles.com/uploads/2016/07/02042549/hbo-700x525.jpg",
            "123456",
            20,
            1,
            "active"
        )
    )

    val medias = arrayOf(
        Media(
            0,
            TYPE_VIDEO,
            1,
            "Final Song",
            Date(),
            "https://i.ytimg.com/vi/WUcXQ--yGWQ/hq720.jpg?sqp=-oaymwEXCKAGEMIDIAQqCwjVARCqCBh4INgESFo&rs=AMzJL3k7QzvpCsvtVBAjFF6VnfTT0pDVhQ"
        ),
        Media(
            1,
            TYPE_VIDEO,
            3,
            "This girl",
            Date(),
            "https://i.ytimg.com/vi/2Y6Nne8RvaA/hq720.jpg"
        ),
        Media(
            2,
            TYPE_MUSIC,
            4,
            "Dark Paradise",
            Date(),
            "https://lh3.googleusercontent.com/qwwpRuY48JasZF3m61zCwVbjREBzTbc-TvJAqKWPJ_PEqKkb_eVITPE1IdadMR-fhMm_QY4j3iRUJ5M=w544-h544-l90-rj"
        ),
        Media(
            3,
            TYPE_MUSIC,
            2,
            "Grip",
            Date(),
            "https://lh3.googleusercontent.com/DUaypfMkSZl6J_6TKwpeYzJuaU57S9vFaJPCKja960Yva-FZLe7YxhCjkfUxXVnzGf6f7b7WxywX7DU=w544-h544-l90-rj"
        )
    )

    val playlists = arrayOf(
        Playlist(
            0,
            "Chill",
            "https://i.ytimg.com/vi/0XFudmaObLI/hq720.jpg",
            listOf(0L, 1L, 2L, 3L)
        ),
        Playlist(1, "S", "https://i.ytimg.com/vi/tRXq-Zzsfic/hq720.jpg", listOf(0L, 1L, 2L, 3L)),
        Playlist(
            2,
            "Beach",
            "https://lh3.googleusercontent.com/HXTZy3WPLr9X0JqakRrrulmoHK3cLs1D2VBZsYul2JvyZ8RCatwGWaDyJE-2OFxfWKu8hFAptITGDRxz=w544-h544-l90-rj",
            listOf(0L, 1L, 2L, 3L)
        ),
        Playlist(
            3,
            "The playlist",
            "https://lh3.googleusercontent.com/m2pZLjozMvQBj21LgvAIslVPP-T2xQlxbxCTJ98vpPN8HZ0fgR-wisJQ2IzrKS2yLTAYBjs0TpOYnIY=w544-h544-l90-rj",
            listOf(0L, 1L, 2L, 3L)
        ),
        Playlist(
            4,
            "Boost",
            "https://lh3.googleusercontent.com/v3OT6H4wt-8vgkPo54-3IoSNTNqjyz_DQ-r2gq5Bc1ln-HT7WQXnaPML6yNw4bKSi_DpZEdKIINiBXub=w544-h544-l90-rj",
            listOf(0L, 1L, 2L, 3L)
        )
    )

    fun sync(database: Database) {
        // Insert
        database.user().upsert(*users)
        database.media().upsert(*medias)
        //database.playlist().upsert(*playlists)
    }
}