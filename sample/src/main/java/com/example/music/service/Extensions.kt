package com.example.music.service

import android.Manifest
import android.accounts.Account
import android.accounts.AccountManager
import android.app.Service
import android.content.ContentResolver
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.os.bundleOf
import com.example.music.BuildConfig


fun Account.triggerRefresh(syncNow: Boolean = true) {
    val extras = bundleOf(
        ContentResolver.SYNC_EXTRAS_MANUAL to syncNow,
        ContentResolver.SYNC_EXTRAS_EXPEDITED to syncNow
    )
    ContentResolver.requestSync(this, BuildConfig.CONTENT_AUTHORITY, extras)
}

fun Context.createSyncAccount(
    accountName: String,
    password: String? = null,
    userdata: Bundle? = null
): Account? {
    // Create the account type and default account
    val newAccount = Account(accountName, BuildConfig.ACCOUNT_TYPE)

    // Get an instance of the Android account manager
    val accountManager = getSystemService(Service.ACCOUNT_SERVICE) as AccountManager

    // Add the account and account type, no password or user data
    return if (accountManager.addAccountExplicitly(newAccount, password, userdata)) {
        // Inform the system that this account supports sync
        ContentResolver.setIsSyncable(newAccount, BuildConfig.CONTENT_AUTHORITY, 1)

        // Inform the system that this account is eligible for auto sync when the network is up
        ContentResolver.setSyncAutomatically(newAccount, BuildConfig.CONTENT_AUTHORITY, true)

        newAccount
    } else {
        // The account exists or some other error occurred. Log this, report it,
        // or handle it internally.
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.GET_ACCOUNTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            accountManager.getAccountsByType(BuildConfig.ACCOUNT_TYPE)[0]
        } else null
    }
}