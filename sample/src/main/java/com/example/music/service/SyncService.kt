package com.example.music.service

import android.accounts.*
import android.app.Service
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.room.Transaction
import com.example.music.data.Database
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class SyncService : Service() {

    @Inject
    lateinit var database: Database

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "Service created")
        synchronized(syncAdapterLock) {
            if (syncAdapter == null) {
                syncAdapter = SyncAdapter(applicationContext, true)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "Service destroyed")
    }

    override fun onBind(intent: Intent): IBinder? = syncAdapter!!.syncAdapterBinder

    private inner class SyncAdapter @JvmOverloads constructor(
        context: Context,
        autoInitialize: Boolean,
        allowParallelSyncs: Boolean = false

    ) : AbstractThreadedSyncAdapter(context, autoInitialize, allowParallelSyncs) {

        @Transaction
        override fun onPerformSync(
            account: Account, extras: Bundle, authority: String,
            provider: ContentProviderClient, syncResult: SyncResult
        ) {
            try {
                Log.i(TAG, "Syncing...")

                // Perform the api calls and save the data to database
                MockSyncService.sync(database)

                Log.i(TAG, "Network synchronization complete")
            } catch (e: OperationCanceledException) {
                Log.e(TAG, e.message!!)
            } catch (e: IOException) {
                Log.e(TAG, e.message!!)
            } catch (e: AuthenticatorException) {
                Log.e(TAG, e.message!!)
            }
        }
    }

    private inner class OnAccountManagerComplete : AccountManagerCallback<Bundle> {
        override fun run(result: AccountManagerFuture<Bundle>) {
            val bundle: Bundle
            try {
                bundle = result.result
            } catch (e: OperationCanceledException) {
                e.printStackTrace()
                return
            } catch (e: AuthenticatorException) {
                e.printStackTrace()
                return
            } catch (e: IOException) {
                e.printStackTrace()
                return
            }

            val authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN)
            Log.d(TAG, "Received authentication token " + authToken!!)
        }
    }

    companion object {
        private const val TAG = "SyncService"
        const val SYNC_FREQUENCY = 60 * 2L // 2 minutes (in seconds)
        const val SYNC_STATUS_MESSAGE = 9324
        private val syncAdapterLock = Any()
        private var syncAdapter: SyncAdapter? = null
    }
}