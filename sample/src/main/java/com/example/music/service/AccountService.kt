package com.example.music.service

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import com.example.music.ui.login.LoginActivity

/**
 * Obtain a handle to the [android.accounts.Account] used for sync in this application.
 *
 *
 * It is important that the accountType specified here matches the value in your sync adapter
 * configuration XML file for android.accounts.AccountAuthenticator (often saved in
 * res/xml/syncadapter.xml). If this is not set correctly, you'll receive an error indicating
 * that "caller uid XXXXX is different than the authenticator's uid".
 *
 * accountType AccountType defined in the configuration XML file for
 * android.accounts.AccountAuthenticator (e.g. res/xml/syncadapter.xml).
 *
 * @return Handle to application's account (not guaranteed to resolve unless addSyncAccount()
 * has been called)
 */
class AccountService : Service() {

    companion object {
        private const val TAG = "AccountService"

        const val ARG_FIREBASE_ID_TOKEN = "FIREBASE_ID_TOKEN"
        const val AUTH_TOKEN_TYPE_BEARER = "Bearer"
    }

    private lateinit var authenticator: Authenticator

    override fun onCreate() {
        //android.os.Debug.waitForDebugger()
        Log.i(TAG, "Service created")
        authenticator = Authenticator(this)
    }

    override fun onDestroy() {
        Log.i(TAG, "Service destroyed")
    }

    override fun onBind(intent: Intent): IBinder? {
        return authenticator.iBinder
    }

    class Authenticator(private val context: Context) : AbstractAccountAuthenticator(context) {

        override fun editProperties(
            accountAuthenticatorResponse: AccountAuthenticatorResponse,
            accountType: String
        ): Bundle? {
            throw UnsupportedOperationException()
        }

        override fun addAccount(
            response: AccountAuthenticatorResponse, accountType: String,
            authTokenType: String?, requiredFeatures: Array<String>?, options: Bundle?
        ): Bundle {

            // Wrap up intent to initiate login activity to request either login or signup return
            // it, which will cause the intent to be run
            val result = Bundle()
            val intent = Intent(context, LoginActivity::class.java)
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
            result.putParcelable(AccountManager.KEY_INTENT, intent)
            return result
        }

        override fun confirmCredentials(
            accountAuthenticatorResponse: AccountAuthenticatorResponse,
            account: Account, result: Bundle?
        ): Bundle? {
            return result
        }

        override fun getAuthToken(
            response: AccountAuthenticatorResponse,
            account: Account, authTokenType: String, options: Bundle
        ): Bundle? {
            return null
        }

        override fun getAuthTokenLabel(authTokenType: String): String? {
            throw UnsupportedOperationException()
        }

        override fun updateCredentials(
            accountAuthenticatorResponse: AccountAuthenticatorResponse,
            account: Account, authTokenType: String?, options: Bundle?
        ): Bundle {
            throw UnsupportedOperationException()
        }

        override fun hasFeatures(
            accountAuthenticatorResponse: AccountAuthenticatorResponse,
            account: Account, strings: Array<String>
        ): Bundle? {
            throw UnsupportedOperationException()
        }
    }
}