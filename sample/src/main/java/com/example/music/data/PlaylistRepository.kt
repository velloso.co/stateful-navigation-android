package com.example.music.data

import com.example.music.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class PlaylistRepository @Inject constructor(
    private val playlistLocalDataSource: PlaylistLocalDataSource,
    private val playlistRemoteDataSource: PlaylistRemoteDataSource,
    @IoDispatcher private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    /**
     * Returns the playlists applying transformations on the flow.
     * These operations are lazy and don't trigger the flow. They just transform
     * the current value emitted by the flow at that point in time.
     */
    fun getPlaylists(username: String): Flow<List<Playlist>> = playlistRemoteDataSource
        .getItems(username)
        // Intermediate operation to save in the cache
        .onEach { items -> saveInCache(items) }
        .catch { exception ->
            emit(listFromCache(username))
            throw exception
        }
        // flowOn affects the upstream flow ↑
        .flowOn(dispatcher)
    // the downstream flow ↓ is not affected

    /**
     * In disk cache implementation
     */
    private fun listFromCache(username: String) =
        playlistLocalDataSource.listItems(username)

    /**
     * In disk cache implementation
     */
    private fun saveInCache(items: List<Playlist>) {
        playlistLocalDataSource.saveItems(items)
    }

    interface PlaylistDataSource {
        fun getItems(username: String): Flow<List<Playlist>>
    }

    class PlaylistLocalDataSource @Inject constructor(
        private val database: Database,
        private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    ) : PlaylistDataSource {

        // Get direct as a flow using Room Flow support
        override fun getItems(username: String): Flow<List<Playlist>> =
            database.playlist().listAsFlow().flowOn(dispatcher)

        fun listItems(username: String): List<Playlist> =
            database.playlist().listNow()

        fun saveItems(items: List<Playlist>) {
            database.playlist().clear()
            database.playlist().upsert(*items.toTypedArray())
        }
    }

    class PlaylistRemoteDataSource @Inject constructor(
        private val apiClient: ApiClient,
        private val autoRefreshIntervalMs: Long = 60_000,
        private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    ) : PlaylistDataSource {

        override fun getItems(username: String): Flow<List<Playlist>> =
            flow {
                while (true) {
                    val items = apiClient.listPlaylist().map {
                        Playlist(
                            id = it.id,
                            title = it.title,
                            image = it.image,
                            mediaIds = it.mediaIds
                        )
                    }
                    // Simulates slow network call
                    delay(3000)
                    emit(items) // Emits the result of the request to the flow
                    delay(autoRefreshIntervalMs) // Suspends the coroutine for some time
                }
            }.flowOn(dispatcher)

        interface ApiClient {

            fun listPlaylist(): List<PlaylistResource>

            data class PlaylistResource(
                var id: Long? = null,

                var title: String? = null,

                var image: String? = null,

                var mediaIds: List<Long> = emptyList()
            )
        }
    }
}






