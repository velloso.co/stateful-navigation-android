package com.example.music.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import co.velloso.android.database.BaseDao
import kotlinx.coroutines.flow.Flow

@Dao
abstract class PlaylistDao : BaseDao<Playlist>() {

    @Query("SELECT * FROM Playlist WHERE id = :id")
    abstract fun get(id: Long): LiveData<Playlist>

    @Query("SELECT * FROM Playlist WHERE id = :id")
    abstract fun getNow(id: Long): Playlist

    @Query("DELETE FROM Playlist")
    abstract fun clear()

    @Query("SELECT * FROM Playlist")
    abstract fun list(): Flow<List<Playlist>>

    @Query("SELECT * FROM Playlist")
    abstract fun listNow(): List<Playlist>

    @Query("SELECT * FROM Playlist")
    abstract fun listAsFlow(): Flow<List<Playlist>>
}