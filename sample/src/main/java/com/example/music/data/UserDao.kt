package com.example.music.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import co.velloso.android.database.BaseDao
import kotlinx.coroutines.flow.Flow

@Dao
abstract class UserDao : BaseDao<User>() {

    @Query("SELECT * FROM User WHERE id = :id")
    abstract fun get(id: Long): LiveData<User>

    @Query("SELECT * FROM User WHERE id = :id")
    abstract fun getAsFlow(id: Long): Flow<User>

    @Query("SELECT * FROM User WHERE email = :email")
    abstract fun getNow(email: String): User

    @Query("SELECT * FROM User")
    abstract fun listAsFlow(): Flow<List<User>>
}