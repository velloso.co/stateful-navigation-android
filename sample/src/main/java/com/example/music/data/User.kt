package com.example.music.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    indices = [
        Index("id"),
        Index("email")
    ]
)
data class User(
    @PrimaryKey
    var id: Long = -1,

    var displayName: String? = null,

    var fullName: String? = null,

    var birthDate: Date? = null,

    var email: String? = null,

    var profilePicture: String? = null,

    var password: String? = null,

    var maxDiscount: Int? = null,

    var companyId: Long? = null,

    var status: String? = null
)