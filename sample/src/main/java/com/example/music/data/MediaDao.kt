package com.example.music.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import co.velloso.android.database.BaseDao
import kotlinx.coroutines.flow.Flow
import java.util.*

@Dao
abstract class MediaDao : BaseDao<Media>() {

    @Query("SELECT * FROM Media WHERE id = :id")
    abstract fun get(id: Long): LiveData<Media>

    @Query("SELECT * FROM Media WHERE id = :id")
    abstract fun getNow(id: Long): Media

    @Query("SELECT * FROM Media")
    abstract fun list(): LiveData<List<Media>>

    @Query("SELECT *, User.displayName as artist FROM Media, User WHERE Media.artistId = User.id")
    abstract fun listWithArtist(): LiveData<List<MediaWithArtist>>

    @Query("SELECT *, User.displayName as artist FROM Media, User WHERE Media.artistId = User.id")
    abstract fun listWithArtistAsFlow(): Flow<List<MediaWithArtist>>

    data class MediaWithArtist(
        var id: Long = -1,
        var type: Int = Media.TYPE_MUSIC,
        var artistId: Long = 0,
        var title: String = "",
        var date: Date? = null,
        var image: String? = null,
        var artist: String = ""
    )
}