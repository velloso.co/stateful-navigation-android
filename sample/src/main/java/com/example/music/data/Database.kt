package com.example.music.data

import androidx.room.RoomDatabase
import androidx.room.Transaction
import androidx.room.TypeConverters
import co.velloso.android.database.GeneralConverters
import co.velloso.android.database.JsonConverters

@androidx.room.Database(
    version = 6,
    entities = [
        Playlist::class,
        Media::class,
        User::class
    ]
)
@TypeConverters(value = [GeneralConverters::class, JsonConverters::class])
abstract class Database : RoomDatabase() {

    abstract fun media(): MediaDao

    abstract fun playlist(): PlaylistDao

    abstract fun user(): UserDao

    @Transaction
    fun doInTransaction(block: ((Database) -> Unit)) {
        block.invoke(this)
    }
}