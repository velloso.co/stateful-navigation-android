package com.example.music.data

//
//class UsersRemoteDataSource(
//    private val apiClient: ApiClient,
//    private val refreshIntervalMs: Long = 5000
//) {
//    val users: Flow<List<UserService.Result>> = flow {
//        while (true) {
//            val users = apiClient.user().list().execute().body()?.items ?: emptyList()
//            emit(users) // Emits the result of the request to the flow
//            delay(refreshIntervalMs) // Suspends the coroutine for some time
//        }
//    }.flowOn(Dispatchers.IO)
//
//}
//
//class UserRepository @Inject constructor(
//    private val usersDataSource: UsersRemoteDataSource,
//    private val database: Database
//) {
//
//    val cachedUsers: MutableList<UserService.Result> = mutableListOf()
//
//    /**
//     * Returns the favorite latest news applying transformations on the flow.
//     * These operations are lazy and don't trigger the flow. They just transform
//     * the current value emitted by the flow at that point in time.
//     */
//    val verifiedUsers: Flow<List<UserService.Result>> = usersDataSource
//        .users
//        // Intermediate operation to filter the list
//        .map { users -> users.filter { it.roles != 0 } }
//        // Intermediate operation to save in the cache
//        .onEach { users -> saveInCache(users) }
//        // flowOn affects the upstream flow ↑
//        .flowOn(Dispatchers.Default)
//        // the downstream flow ↓ is not affected
//        .catch { exception -> }
//
//    suspend fun getCurrentUser(): Response<UserService.Resource> {
//        // Move the execution of the coroutine to the I/O dispatcher
//        return withContext(Dispatchers.IO) {
//            // Blocking network request code
//            ApiClient.user().get(0).execute()
//        }
//    }
//
//    private fun saveInCache(users: List<UserService.Result>) {
//        cachedUsers.clear()
//        cachedUsers.addAll(users)
//    }
//
//}
//
//class UsersViewModel(
//    private val newsRepository: UserRepository
//) : ViewModel() {
//
//    init {
//        viewModelScope.launch {
//            // Trigger the flow and consume its elements using collect
//            newsRepository.verifiedUsers
//                .catch { exception -> }
//                .collect { users ->
//                    // Update View with the latest favorite news
//                }
//        }
//    }
//}

//val continuation = WorkManager.getInstance(context)
//    .beginUniqueWork(
//        Constants.IMAGE_MANIPULATION_WORK_NAME,
//        ExistingWorkPolicy.REPLACE,
//        OneTimeWorkRequest.from(CleanupWorker::class.java)
//    ).then(OneTimeWorkRequest.from(WaterColorFilterWorker::class.java))
//    .then(OneTimeWorkRequest.from(GrayScaleFilterWorker::class.java))
//    .then(OneTimeWorkRequest.from(BlurEffectFilterWorker::class.java))
//    .then(
//        if (save) {
//            workRequest<SaveImageToGalleryWorker>(tag = Constants.TAG_OUTPUT)
//        } else /* upload */ {
//            workRequest<UploadWorker>(tag = Constants.TAG_OUTPUT)
//        }
//    )

