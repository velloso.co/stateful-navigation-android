package com.example.music.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    indices = [
        Index("id")
    ]
)
data class Playlist(
    @PrimaryKey
    var id: Long? = null,

    var title: String? = null,

    var image: String? = null,

    var mediaIds: List<Long> = emptyList()
)