package com.example.music.data

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.music.data.PreferenceStorage.PreferencesKeys.PREF_CONFERENCE_TIME_ZONE
import com.example.music.data.PreferenceStorage.PreferencesKeys.PREF_HAS_LOGOUT_EXPLICITLY
import com.example.music.data.PreferenceStorage.PreferencesKeys.PREF_NOTIFICATIONS_SHOWN
import com.example.music.data.PreferenceStorage.PreferencesKeys.PREF_RECEIVE_NOTIFICATIONS
import com.example.music.data.PreferenceStorage.PreferencesKeys.PREF_SELECTED_THEME
import com.example.music.data.PreferenceStorage.PreferencesKeys.PREF_SEND_USAGE_STATISTICS
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Storage for app and user preferences.
 */
@Singleton
class PreferenceStorage @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {
    val logoutExplicitly = dataStore.data.map { preferences ->
        // No type safety.
        preferences[PREF_HAS_LOGOUT_EXPLICITLY] ?: false
    }

    suspend fun setHasLoggedOutExplicitly(isLoggedIn: Boolean) {
        dataStore.edit { settings ->
            settings[PREF_HAS_LOGOUT_EXPLICITLY] = isLoggedIn
        }
    }

    suspend fun showNotificationsPreference(show: Boolean) {
        dataStore.edit {
            it[PREF_NOTIFICATIONS_SHOWN] = show
        }
    }

    val notificationsPreferenceShown = dataStore.data.map {
        it[PREF_NOTIFICATIONS_SHOWN] ?: false
    }

    suspend fun preferToReceiveNotifications(prefer: Boolean) {
        dataStore.edit {
            it[PREF_RECEIVE_NOTIFICATIONS] = prefer
        }
    }

    val preferToReceiveNotifications = dataStore.data.map {
        it[PREF_RECEIVE_NOTIFICATIONS] ?: false
    }

    suspend fun sendUsageStatistics(send: Boolean) {
        dataStore.edit {
            it[PREF_SEND_USAGE_STATISTICS] = send
        }
    }

    val sendUsageStatistics = dataStore.data.map {
        it[PREF_SEND_USAGE_STATISTICS] ?: true
    }

    suspend fun preferConferenceTimeZone(preferConferenceTimeZone: Boolean) {
        dataStore.edit {
            it[PREF_CONFERENCE_TIME_ZONE] = preferConferenceTimeZone
        }
    }

    val preferConferenceTimeZone = dataStore.data.map { it[PREF_CONFERENCE_TIME_ZONE] ?: true }

    suspend fun selectTheme(theme: String) {
        dataStore.edit {
            it[PREF_SELECTED_THEME] = theme
        }
    }

    val selectedTheme = dataStore.data.map { it[PREF_SELECTED_THEME] ?: Theme.SYSTEM.storageKey }

    /**
     * Represents the available UI themes for the application
     */
    enum class Theme(val storageKey: String) {
        LIGHT("light"),
        DARK("dark"),
        SYSTEM("system"),
        BATTERY_SAVER("battery_saver")
    }

    object PreferencesKeys {
        val PREF_HAS_LOGOUT_EXPLICITLY = booleanPreferencesKey("pref_has_logout_explicitly")
        val PREF_NOTIFICATIONS_SHOWN = booleanPreferencesKey("pref_notifications_shown")
        val PREF_RECEIVE_NOTIFICATIONS = booleanPreferencesKey("pref_receive_notifications")
        val PREF_SEND_USAGE_STATISTICS = booleanPreferencesKey("pref_send_usage_statistics")
        val PREF_CONFERENCE_TIME_ZONE = booleanPreferencesKey("pref_conference_time_zone")
        val PREF_SELECTED_THEME = stringPreferencesKey("pref_dark_mode")
    }

    companion object {
        const val PREFS_NAME = "main"

        val Context.dataStore by preferencesDataStore(
            name = PREFS_NAME
        )

        /**
         * Returns the matching [Theme] for the given [storageKey] value.
         */
        fun themeFromStorageKey(storageKey: String): Theme? {
            return Theme.values().firstOrNull { it.storageKey == storageKey }
        }

        @SuppressLint("WrongConstant")
        fun AppCompatActivity.updateForTheme(theme: Theme) = when (theme) {
            Theme.DARK -> delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_YES
            Theme.LIGHT -> delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_NO
            Theme.SYSTEM -> delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            Theme.BATTERY_SAVER -> delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY
        }
    }
}