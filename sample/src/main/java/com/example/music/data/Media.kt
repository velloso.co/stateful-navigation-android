package com.example.music.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

@Entity(
    indices = [
        Index("id")
    ]
)
data class Media(
    @PrimaryKey
    var id: Long = -1,

    var type: Int = TYPE_MUSIC,

    var artistId: Long = 0,

    var title: String = "",

    var date: Date? = null,

    var image: String? = null
) {
    companion object {
        const val TYPE_MUSIC = 0
        const val TYPE_VIDEO = 1
    }
}