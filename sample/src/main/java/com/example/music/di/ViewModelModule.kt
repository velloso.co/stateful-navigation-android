package com.example.music.di

import com.example.music.data.Database
import com.example.music.data.PlaylistRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import kotlinx.coroutines.CoroutineDispatcher

@InstallIn(ViewModelComponent::class)
@Module
class ViewModelModule {

    @Provides
    fun providePlaylistRemoteDataSource(
        apiClient: PlaylistRepository.PlaylistRemoteDataSource.ApiClient
    ): PlaylistRepository.PlaylistRemoteDataSource {
        return PlaylistRepository.PlaylistRemoteDataSource(apiClient)
    }

    @Provides
    fun providePlaylistLocalDataSource(
        database: Database,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): PlaylistRepository.PlaylistLocalDataSource {
        return PlaylistRepository.PlaylistLocalDataSource(database, ioDispatcher)
    }
}