package com.example.music.di

import android.accounts.AccountManager
import android.content.Context
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.room.Room
import com.example.music.data.Database
import com.example.music.data.PlaylistRepository
import com.example.music.data.PreferenceStorage.Companion.PREFS_NAME
import com.example.music.service.MockSyncService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ApplicationModule {

    @Provides
    fun provideWifiManager(@ApplicationContext context: Context): WifiManager =
        context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager

    @Provides
    fun provideConnectivityManager(@ApplicationContext context: Context): ConnectivityManager =
        context.applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager

    @Provides
    fun provideAccountManager(@ApplicationContext context: Context): AccountManager =
        context.applicationContext.getSystemService(Context.ACCOUNT_SERVICE)
                as AccountManager

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext applicationContext: Context): Database {
        return Room.databaseBuilder(applicationContext, Database::class.java, "main.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideApiClient(): PlaylistRepository.PlaylistRemoteDataSource.ApiClient {
        return object : PlaylistRepository.PlaylistRemoteDataSource.ApiClient {
            override fun listPlaylist(): List<PlaylistRepository.PlaylistRemoteDataSource.ApiClient.PlaylistResource> {
                return MockSyncService.playlists.map {
                    PlaylistRepository.PlaylistRemoteDataSource.ApiClient.PlaylistResource(
                        id = it.id, title = it.title, image = it.image, mediaIds = it.mediaIds
                    )
                }
            }
        }
    }

    @ApplicationScope
    @Singleton
    @Provides
    fun providesApplicationScope(
        @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
    ): CoroutineScope = CoroutineScope(SupervisorJob() + defaultDispatcher)

    @Singleton
    @Provides
    fun provideDataStore(
        @ApplicationContext context: Context,
        @ApplicationScope applicationScope: CoroutineScope
    ): DataStore<Preferences> = PreferenceDataStoreFactory.create(
        migrations = listOf(SharedPreferencesMigration(context, PREFS_NAME)),
        scope = applicationScope
    ) {
        context.preferencesDataStoreFile(PREFS_NAME)
    }
}