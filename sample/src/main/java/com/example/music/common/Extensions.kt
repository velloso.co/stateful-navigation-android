package com.example.music.common

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setImageOptimized(imageUrl: String?) {
    if (imageUrl != null) Glide.with(context).load(imageUrl).into(this)
}