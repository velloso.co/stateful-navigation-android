package com.example.music.common

import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Application : android.app.Application()