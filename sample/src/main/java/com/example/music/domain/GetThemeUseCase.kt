package com.example.music.domain

import android.os.Build
import com.example.music.data.PreferenceStorage
import com.example.music.data.PreferenceStorage.Companion.themeFromStorageKey
import com.example.music.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class GetThemeUseCase @Inject constructor(
    private val preferenceStorage: PreferenceStorage,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : UseCase<Unit, PreferenceStorage.Theme>(dispatcher) {

    override fun execute(parameters: Unit): Flow<Result<PreferenceStorage.Theme>> {
        return preferenceStorage.selectedTheme.map {
            val theme = themeFromStorageKey(it)
                ?: when {
                    Build.VERSION.SDK_INT >= 29 -> PreferenceStorage.Theme.SYSTEM
                    else -> PreferenceStorage.Theme.BATTERY_SAVER
                }
            Result.Success(theme)
        }
    }
}