package com.example.music.domain

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn

abstract class UseCase<in P, R>(private val coroutineDispatcher: CoroutineDispatcher) {

    operator fun invoke(parameters: P): Flow<Result<R>> = execute(parameters)
        .catch { e -> emit(Result.Error(Exception(e))) }
        .flowOn(coroutineDispatcher)

    /**
     * Override this to set the code to be executed.
     */
    protected abstract fun execute(parameters: P): Flow<Result<R>>


    /**
     * A generic class that holds a value with its loading status.
     * @param <T>
     */
    sealed class Result<out R> {

        data class Success<out T>(val data: T) : Result<T>()
        data class Error(val exception: Exception) : Result<Nothing>()
        object Loading : Result<Nothing>()

        override fun toString(): String {
            return when (this) {
                is Success<*> -> "Success[data=$data]"
                is Error -> "Error[exception=$exception]"
                Loading -> "Loading"
            }
        }

        companion object {
            /**
             * `true` if [Result] is of type [Success] & holds non-null [Success.data].
             */
            val Result<*>.succeeded
                get() = this is Result.Success && data != null

            fun <T> Result<T>.successOr(fallback: T): T {
                return (this as? Success<T>)?.data ?: fallback
            }

            val <T> Result<T>.data: T?
                get() = (this as? Success)?.data

            /**
             * Updates value of [liveData] if [Result] is of type [Success]
             */
            inline fun <reified T> Result<T>.updateOnSuccess(liveData: MutableLiveData<T>) {
                if (this is Success) {
                    liveData.value = data
                }
            }

            /**
             * Updates value of [MutableStateFlow] if [Result] is of type [Success]
             */
            inline fun <reified T> Result<T>.updateOnSuccess(stateFlow: MutableStateFlow<T>) {
                if (this is Success) {
                    stateFlow.value = data
                }
            }
        }
    }
}