package com.example.music.ui.start

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.music.data.PlaylistRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StartViewModel @Inject constructor(
    private val playlistRepository: PlaylistRepository
) : ViewModel() {

    private val _uiState: MutableStateFlow<StartFragment.State> = MutableStateFlow(
        StartFragment.State.Loading(0)
    )
    val uiState: StateFlow<StartFragment.State> = _uiState.asStateFlow()

    private var refreshJob: Job? = null

    fun refresh(username: String) {
        refreshJob?.cancel()
        refreshJob = viewModelScope.launch {
            playlistRepository.getPlaylists(username)
                .catch { e ->
                    _uiState.value = StartFragment.State.Error(exception = e)
                }
                .collect { items ->
                    _uiState.value = StartFragment.State.Success(items = items)
                }
        }
    }
}