package com.example.music.ui.detail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.music.R
import com.example.music.common.setImageOptimized
import com.example.music.databinding.DetailBinding
import com.example.music.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class DetailFragment : Fragment(R.layout.detail) {

    private val mainViewModel: MainViewModel by activityViewModels()

    private val binding: DetailBinding by viewBinding(DetailBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.setOnClickListener {
            Toast.makeText(requireContext(), "Button pressed", Toast.LENGTH_LONG).show()
        }

        mainViewModel.playlists.flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach { items ->
                items.find { it.id == arguments?.getLong(ARG_ID) ?: 1L }?.let { item ->
                    binding.postImageView.setImageOptimized(item.image!!)
                    binding.datetimeTextView.text = item.title
                    binding.postTitleTextView.text = item.title
                }
            }
            .launchIn(lifecycleScope)
    }

    companion object {
        const val ARG_ID = "ARG_ID"
    }
}
