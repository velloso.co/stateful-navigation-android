package com.example.music.ui.login

import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.NavHost
import by.kirich1409.viewbindingdelegate.viewBinding
import co.velloso.android.statefulnavigation.navigators.ViewNavigator
import co.velloso.android.statefulnavigation.navigators.ViewNavigator.Companion.EXTRAS_SHARED_ELEMENT_CONTAINER
import com.example.music.R
import com.example.music.data.User
import com.example.music.databinding.SignupDialogBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Screen of sign up. It shows how to use ViewNavigator.
 */
@AndroidEntryPoint
class SignUpDialogFragment : DialogFragment(R.layout.signup_dialog), NavHost {

    private val viewModel: LoginViewModel by viewModels()

    // navigation graph Id
    private val graphId: Int = R.navigation.navigation_signup

    private var navState: Bundle? = null

    private val navControllerInternal by lazy {
        val navController = createNavController()

        if (navState != null) {
            // Navigation controller state overrides arguments
            navController.restoreState(navState)
        }
        // Set by overriding the graphId field
        navController.setGraph(graphId)

        navController
    }

    override val navController: NavController
        get() = navControllerInternal

    private val listener by lazy {
        activity as LoginDialogFragmentListener
    }

    private val extras by lazy {
        ViewNavigator.extrasOf(EXTRAS_SHARED_ELEMENT_CONTAINER to binding.dialogContainer)
    }

    interface LoginDialogFragmentListener {
        fun onCancel() {}
        fun onComplete(user: User, dialog: Dialog) {}
    }

    private val binding: SignupDialogBinding by viewBinding(SignupDialogBinding::bind)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
        if (savedInstanceState != null) {
            navState = savedInstanceState.getBundle(KEY_NAV_CONTROLLER_STATE)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize conditional next button
        binding.nextButton.setOnClickListener {
            onNext(view)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val navState = this.navController.saveState()
        if (navState != null) {
            outState.putBundle(KEY_NAV_CONTROLLER_STATE, navState)
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        // Initialize display name view by navigating to it
        val destinationId = if (savedInstanceState == null) {
            R.id.navigation_login_dialog_question1
        } else navController.currentDestination!!.id
        navController.navigate(destinationId, null, null, extras)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window!!.setLayout(MATCH_PARENT, MATCH_PARENT)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnKeyListener { _, keyCode, event ->

            // Dispatch the back press button to activity to handle
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                onPrevious()
                if (!navController.navigateUp()) activity?.onBackPressed()
                return@setOnKeyListener true
            }

            false
        }
        return dialog
    }

    private fun createNavController() = NavController(requireContext()).apply {
        navigatorProvider.addNavigator(ViewNavigator(layoutInflater))
    }

    private fun onNext(view: View) {
        when (navController.currentDestination?.id) {
            R.id.navigation_login_dialog_question1 -> {
                val displayNameEditText = view.findViewById<EditText>(R.id.displayNameEditText)
                viewModel.displayName = displayNameEditText.text.toString()
                navController.navigate(R.id.navigation_login_dialog_question2, null, null, extras)
            }
            R.id.navigation_login_dialog_question2 -> {
                val emailEditText = view.findViewById<EditText>(R.id.emailEditText)
                viewModel.email = emailEditText.text.toString()
                navController.navigate(R.id.navigation_login_dialog_question3, null, null, extras)
                binding.nextButton.setText(R.string.confirmation_signup)
            }
            R.id.navigation_login_dialog_question3 -> {
                val user = User(displayName = viewModel.displayName)
                listener.onComplete(user, dialog!!)
            }
        }
    }

    private fun onPrevious() {
        binding.nextButton.setText(R.string.next)
    }

    companion object {
        private const val KEY_NAV_CONTROLLER_STATE =
            "co.velloso.example.navigation:dialog-fragment:navControllerState"
    }
}