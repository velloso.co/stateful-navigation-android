package com.example.music.ui.login

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.FragmentNavigator
import co.velloso.android.statefulnavigation.activity.NavHostActivity
import co.velloso.android.statefulnavigation.navigators.DialogFragmentNavigator
import co.velloso.android.statefulnavigation.navigators.StatefulFragmentNavigator
import co.velloso.android.statefulnavigation.navigators.ViewNavigator
import com.example.music.R
import com.example.music.data.Database
import com.example.music.data.User
import com.example.music.service.createSyncAccount
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : NavHostActivity(), LoginFragment.LoginFragmentListener,
    SignUpDialogFragment.LoginDialogFragmentListener {

    @Inject
    lateinit var database: Database

    override var graphId = R.navigation.navigation_login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val view = FrameLayout(this).apply { id = R.id.container }
        setContentView(view)
    }

    override fun onCreateNavController() = NavController(this).apply {
        navigatorProvider.addNavigator(
            StatefulFragmentNavigator(
                this@LoginActivity,
                supportFragmentManager,
                R.id.container
            )
        )
        navigatorProvider.addNavigator(
            FragmentNavigator(
                this@LoginActivity,
                supportFragmentManager,
                R.id.container
            )
        )
        navigatorProvider.addNavigator(
            DialogFragmentNavigator(
                this@LoginActivity,
                supportFragmentManager
            )
        )
        navigatorProvider.addNavigator(ViewNavigator(layoutInflater))
    }

    override fun onSupportNavigateUp() = navController.navigateUp()

    override fun onBackPressed() {
        when {
            navController.navigateUp() && navController.currentDestination?.id == R.id.loginFragment -> {
                findViewById<View>(R.id.container).visibility = VISIBLE
            }
            else -> super.onBackPressed()
        }
    }

    override fun onComplete(user: User, dialog: Dialog) {
        database.user().insert(user) {
            createSyncAccount(user.email ?: "None")
            dialog.dismiss()
            navController.navigate(R.id.nav_destination_main)
            finish()
        }
    }

    override fun onSignIn() {
        navController.navigate(R.id.nav_action_to_main)
        finish()
    }

    override fun onSignUp() {
        findViewById<View>(R.id.container).visibility = GONE
        navController.navigate(R.id.nav_action_to_login_dialog)
    }
}
