package com.example.music.ui.account

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.music.R
import com.example.music.data.PreferenceStorage
import com.example.music.databinding.AccountBinding
import com.example.music.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class AccountFragment : Fragment(R.layout.account) {

    private val mainViewModel: MainViewModel by activityViewModels()

    private val viewModel: AccountViewModel by activityViewModels()

    private var _binding: AccountBinding? = null
    private val binding: AccountBinding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = AccountBinding.bind(view)

        // User data
        viewModel.users.flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach { items ->
                val currentUser = items.first()
                binding.displayNameTextView.text = currentUser.displayName
            }
            .launchIn(lifecycleScope)

        // Theme preference
        binding.themeSwitch.isChecked = mainViewModel.theme.value == PreferenceStorage.Theme.DARK
        mainViewModel.theme.flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach { item ->
                binding.themeSwitch.isChecked = item == PreferenceStorage.Theme.DARK
            }
            .launchIn(lifecycleScope)
        binding.themeSwitch.setOnCheckedChangeListener { _, isChecked ->
            val theme = if (isChecked) PreferenceStorage.Theme.DARK
            else PreferenceStorage.Theme.SYSTEM
            viewModel.updateTheme(theme)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
