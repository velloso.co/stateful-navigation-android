package com.example.music.ui.search

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ListAdapter
import by.kirich1409.viewbindingdelegate.viewBinding
import co.velloso.android.statefulnavigation.extensions.activityNavController
import co.velloso.android.statefulrecyclerview.BindableViewHolder
import com.example.music.R
import com.example.music.common.setImageOptimized
import com.example.music.data.MediaDao
import com.example.music.databinding.SearchBinding
import com.example.music.databinding.SearchGenreItemBinding
import com.example.music.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class SearchFragment : Fragment(R.layout.search) {

    private val navController by activityNavController()

    private val mainViewModel: MainViewModel by activityViewModels()

    private val binding: SearchBinding by viewBinding(SearchBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // List
        binding.searchRecyclerView.layoutManager = GridLayoutManager(view.context, 2)
        mainViewModel.medias.flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach { medias ->
                binding.searchRecyclerView.adapter = GenreAdapter {
                    navController.navigate(R.id.detailFragment)
                }.apply {
                    submitList(medias)
                }
            }
            .launchIn(lifecycleScope)
    }

    class GenreAdapter(val onClick: ((MediaDao.MediaWithArtist) -> Unit)? = null) :
        ListAdapter<MediaDao.MediaWithArtist, BindableViewHolder>(
            object : DiffUtil.ItemCallback<MediaDao.MediaWithArtist>() {
                override fun areItemsTheSame(
                    oldItem: MediaDao.MediaWithArtist,
                    newItem: MediaDao.MediaWithArtist
                ) =
                    oldItem.id == newItem.id

                override fun areContentsTheSame(
                    oldItem: MediaDao.MediaWithArtist,
                    newItem: MediaDao.MediaWithArtist
                ) =
                    oldItem == newItem
            }
        ) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemViewHolder(parent)

        override fun onBindViewHolder(holder: BindableViewHolder, position: Int) {
            holder.bind()
        }

        inner class ItemViewHolder(parent: ViewGroup) :
            BindableViewHolder(parent, R.layout.search_genre_item) {
            override fun bind() {
                val item = getItem(bindingAdapterPosition)
                val binding = SearchGenreItemBinding.bind(itemView)
                binding.tileImageView.setImageOptimized(item.image!!)
                binding.tileTitleTextView.text = item.title
                binding.root.setOnClickListener {
                    onClick?.invoke(item)
                }
            }
        }
    }
}
