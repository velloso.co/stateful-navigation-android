package com.example.music.ui.player

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.view.AbsSavedState
import android.view.View
import android.widget.FrameLayout
import co.velloso.android.behaviors.getBehavior
import co.velloso.android.statefulnavigation.BottomSheetListener
import com.example.music.R
import com.example.music.common.setImageOptimized
import com.example.music.data.MediaDao
import com.example.music.databinding.PlayerBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_HIDDEN
import kotlin.math.max

class PlayerBottomSheet @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0

) : FrameLayout(context, attrs, defStyleAttr), BottomSheetListener {

    var media: MediaDao.MediaWithArtist? = null

    var time: Int = 0

    var binding: PlayerBinding

    init {
        val view = inflate(context, R.layout.player, this)
        binding = PlayerBinding.bind(view)

        // Initialize the clicks on the peek toolbar that is visible when collapsed
        binding.playerToolbar.inflateMenu(R.menu.menu_bottom_sheet)
        binding.playerToolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.action_close -> {
                    stopMedia()
                    true
                }
                R.id.action_play -> {
                    media?.let {
                        playMedia(it, time)
                    }
                    true
                }
                else -> false
            }
        }
    }

    override fun onSlide(progress: Float) {
        dispatchUpdate(binding.mediaFrameLayout, progress)
    }

    fun playMedia(media: MediaDao.MediaWithArtist, startTime: Int = 30) {
        this.media = media
        this.time = startTime
        binding.mediaDisplayImageView.setImageOptimized(media.image ?: "")
        binding.mediaTitleTextView.text = media.title
        binding.mediaSubtitleTextView.text = media.artist
        binding.playerToolbar.title = media.title
        binding.playerToolbar.subtitle = media.artist
        binding.playerSeekBar.progress = startTime

        visibility = View.VISIBLE
        getBehavior().state = STATE_EXPANDED
        getBehavior().isHideable = false
    }

    fun pauseMedia(pauseTime: Int) {
        time = pauseTime
    }

    fun stopMedia() {
        media = null
        time = 0
        getBehavior().isHideable = true
        getBehavior().state = STATE_HIDDEN
    }

    private val expandedMediaHeight by lazy {
        binding.mediaFrameLayout.measuredHeight
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        expandedMediaHeight
    }

    private fun dispatchUpdate(child: View, progress: Float) {
        val peekHeight = getBehavior().peekHeight / 2
        val height = (expandedMediaHeight - peekHeight).times(progress) + peekHeight
        child.layoutParams.height = max(height.toInt(), 0)
        child.requestLayout()
    }

    override fun onSaveInstanceState(): Parcelable {
        val superState = super.onSaveInstanceState()
        val savedState = SavedState(superState)
        savedState.behaviorState = getBehavior().state
        return savedState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        super.onRestoreInstanceState(state)
        getBehavior().state = (state as? SavedState)?.behaviorState ?: STATE_HIDDEN
    }

    class SavedState(superState: Parcelable? = AbsSavedState.EMPTY_STATE) :
        View.BaseSavedState(superState) {
        var behaviorState: Int = STATE_HIDDEN
        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeValue(behaviorState)
        }
    }
}