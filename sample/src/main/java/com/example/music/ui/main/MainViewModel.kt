package com.example.music.ui.main

import androidx.lifecycle.ViewModel
import com.example.music.data.Database
import com.example.music.data.MediaDao
import com.example.music.data.Playlist
import com.example.music.data.PreferenceStorage
import com.example.music.di.ApplicationScope
import com.example.music.domain.GetThemeUseCase
import com.example.music.domain.UseCase.Result.Companion.successOr
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    @ApplicationScope externalScope: CoroutineScope,
    private val database: Database,
    getThemeUseCase: GetThemeUseCase
) : ViewModel() {

    val theme: StateFlow<PreferenceStorage.Theme> = getThemeUseCase(Unit)
        .map { it.successOr(PreferenceStorage.Theme.SYSTEM) }
        .stateIn(externalScope, SharingStarted.Eagerly, PreferenceStorage.Theme.SYSTEM)

    val playlists: Flow<List<Playlist>> by lazy {
        database.playlist().listAsFlow()
    }

    val medias: Flow<List<MediaDao.MediaWithArtist>> by lazy {
        database.media().listWithArtistAsFlow()
    }
}