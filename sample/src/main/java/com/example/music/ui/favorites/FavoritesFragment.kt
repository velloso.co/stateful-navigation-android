package com.example.music.ui.favorites

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavOptions
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import co.velloso.android.extensions.getDrawableCompat
import co.velloso.android.extensions.setTintCompat
import co.velloso.android.statefulnavigation.extensions.activityNavController
import co.velloso.android.statefulrecyclerview.BindableViewHolder
import com.example.music.R
import com.example.music.data.Media.Companion.TYPE_VIDEO
import com.example.music.databinding.FavoritesBinding
import com.example.music.databinding.FavoritesMenuItemBinding
import com.example.music.ui.common.TilesAdapter
import com.example.music.ui.main.MainActivity
import com.example.music.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class FavoritesFragment : Fragment(R.layout.favorites) {

    private val mainViewModel: MainViewModel by activityViewModels()

    private val navController by activityNavController()

    private val menu = listOf(
        FavoriteItem(R.drawable.ic_offline_pin, R.string.downloaded),
        FavoriteItem(R.drawable.ic_playlist_play, R.string.playlists),
        FavoriteItem(R.drawable.ic_album, R.string.albuns),
        FavoriteItem(R.drawable.ic_thumb_up, R.string.titles_that_i_liked),
        FavoriteItem(R.drawable.ic_people, R.string.artists)
    )

    private val binding: FavoritesBinding by viewBinding(FavoritesBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Toolbar
        binding.toolbar.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_notifications, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean = when (menuItem.itemId) {
                R.id.action_notifications -> {
                    Toast.makeText(requireContext(), menuItem.title ?: "None", Toast.LENGTH_LONG)
                        .show()
                    true
                }
                else -> false
            }
        })

        // Recently listen
        binding.section.sectionMediaRecyclerView.layoutManager =
            LinearLayoutManager(view.context, RecyclerView.HORIZONTAL, false)
        mainViewModel.medias.flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach { medias ->
                binding.section.sectionMediaRecyclerView.adapter = TilesAdapter().apply {
                    submitList(medias.map { mediaWithArtist ->
                        TilesAdapter.Tile(
                            id = mediaWithArtist.id,
                            image = mediaWithArtist.image ?: "",
                            title = mediaWithArtist.title,
                            subtitle = mediaWithArtist.artist,
                            isSquare = mediaWithArtist.type != TYPE_VIDEO,
                            listener = {
                                (activity as? MainActivity)?.binding?.playerView
                                    ?.playMedia(mediaWithArtist)
                                navController.navigate(R.id.playerBottomSheet)
                            }
                        )
                    })
                }
            }
            .launchIn(lifecycleScope)


        // Favorites list
        binding.menuRecyclerView.layoutManager = LinearLayoutManager(view.context)
        val drawableEnd = view.context.getDrawableCompat(R.drawable.ic_keyboard_arrow_right)!!
        drawableEnd.setTintCompat(Color.WHITE)
        val navOptions = NavOptions.Builder()
            .setEnterAnim(R.anim.nav_default_enter_anim)
            .setExitAnim(R.anim.nav_default_exit_anim)
            .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
            .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
            .build()
        binding.menuRecyclerView.adapter = MenuAdapter(drawableEnd) { item ->
            navController.navigate(item.navActionId, null, navOptions, null)
        }.apply {
            submitList(menu)
        }
    }

    data class FavoriteItem(
        val iconId: Int,
        val titleId: Int,
        val navActionId: Int = R.id.nav_action_to_detail
    ) {
        object Differ : DiffUtil.ItemCallback<FavoriteItem>() {
            override fun areItemsTheSame(oldItem: FavoriteItem, newItem: FavoriteItem) =
                oldItem.titleId == newItem.titleId

            override fun areContentsTheSame(oldItem: FavoriteItem, newItem: FavoriteItem) =
                oldItem == newItem
        }
    }

    class MenuAdapter(
        val drawableEnd: Drawable,
        val onClick: ((FavoriteItem) -> Unit)? = null
    ) : ListAdapter<FavoriteItem, BindableViewHolder>(FavoriteItem.Differ) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemViewHolder(parent)

        override fun onBindViewHolder(holder: BindableViewHolder, position: Int) {
            holder.bind()
        }

        inner class ItemViewHolder(val parent: ViewGroup) :
            BindableViewHolder(parent, R.layout.favorites_menu_item) {

            override fun bind() {
                val item = getItem(bindingAdapterPosition)
                val binding = FavoritesMenuItemBinding.bind(itemView)
                val drawableStart = itemView.context.getDrawableCompat(item.iconId)!!
                drawableStart.setTintCompat(Color.WHITE)
                binding.menuTitleTextView.setCompoundDrawablesWithIntrinsicBounds(
                    drawableStart, null, drawableEnd, null
                )
                binding.menuTitleTextView.setText(item.titleId)
                binding.menuTitleTextView.setOnClickListener {
                    onClick?.invoke(item)
                }
            }
        }
    }
}
