package com.example.music.ui.start

import android.os.Bundle
import android.os.Parcelable
import android.util.SparseArray
import android.view.*
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import by.kirich1409.viewbindingdelegate.viewBinding
import co.velloso.android.statefulnavigation.extensions.activityNavController
import co.velloso.android.statefulrecyclerview.BindableViewHolder
import co.velloso.android.statefulrecyclerview.StatefulRecyclerAdapter
import co.velloso.android.statefulrecyclerview.StatefulViewHolder
import com.example.music.R
import com.example.music.data.Playlist
import com.example.music.databinding.SectionBinding
import com.example.music.databinding.StartBinding
import com.example.music.ui.common.TilesAdapter
import com.example.music.ui.detail.DetailFragment.Companion.ARG_ID
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class StartFragment : Fragment(R.layout.start) {

    private val startViewModel: StartViewModel by viewModels()

    private val navController by activityNavController()

    private val sections = listOf(
        Section(0, "", "Clips recommandés"),
        Section(1, "Réécouter", "Musique pour l'àpres-midi"),
        Section(2, "", "Vous favoris"),
        Section(3, "Similaire à", "Robin Schulz"),
        Section(4, "", "Nouvelles vidéos"),
        Section(5, "Similaire à", "The Lumineers"),
        Section(6, "", "Meilleurs classements")
    )

    private val binding: StartBinding by viewBinding(StartBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Toolbar
        binding.toolbar.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_start, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean = when (menuItem.itemId) {
                R.id.action_account -> {
                    navController.navigate(R.id.accountFragment)
                    true
                }
                else -> false
            }
        })

        // Sections
        binding.startRecyclerView.layoutManager = LinearLayoutManager(view.context)
        val adapter = StartAdapter { playlist ->
            navController.navigate(R.id.detailFragment, bundleOf(ARG_ID to playlist.id))
        }
        binding.startRecyclerView.adapter = adapter

        // Subscribe UI
        startViewModel.refresh("Any")
        startViewModel.uiState.flowWithLifecycle(viewLifecycleOwner.lifecycle)
            .onEach { state ->
                when (state) {
                    is State.Success -> {
                        updateAdapter(adapter, state.items)
                        binding.progressBar.isVisible = false
                    }
                    is State.Error -> {
                        val message = state.exception.message ?: getString(R.string.error_unknown)
                        Snackbar.make(view, message, 3000).show()
                    }
                    is State.Loading -> {
                        binding.progressBar.isVisible = true
                    }
                }
            }
            .launchIn(lifecycleScope)
    }

    private fun updateAdapter(adapter: StartAdapter, playlists: List<Playlist>) {
        sections.forEach { it.playlists = playlists }
        adapter.submitList(sections)
    }

    data class Section(
        val id: Long = 0,
        val overTitle: String = "",
        val title: String = "",
        var playlists: List<Playlist> = emptyList()
    ) {
        object Differ : DiffUtil.ItemCallback<Section>() {
            override fun areItemsTheSame(oldItem: Section, newItem: Section) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Section, newItem: Section) =
                oldItem == newItem
        }
    }

    class StartAdapter(val onClick: ((Playlist) -> Unit)? = null) :
        StatefulRecyclerAdapter<Section, BindableViewHolder>(
            Section.Differ
        ) {

        init {
            setHasStableIds(true)
        }

        override fun getItemId(position: Int): Long = getItem(position).id

        override fun onBindViewHolder(holder: BindableViewHolder, position: Int) {
            holder.bind()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            SectionViewHolder(parent)

        inner class SectionViewHolder(parent: ViewGroup) :
            StatefulViewHolder(parent, R.layout.section) {

            private val binding = SectionBinding.bind(itemView)

            override fun bind() {
                val section = getItem(bindingAdapterPosition)
                binding.sectionOverTitleTextView.text = section.overTitle
                binding.sectionOverTitleTextView.letterSpacing = .04f
                binding.sectionTitleTextView.text = section.title
                binding.sectionMediaRecyclerView.layoutManager =
                    LinearLayoutManager(itemView.context, HORIZONTAL, false)
                val adapter = TilesAdapter()
                adapter.submitList(section.playlists.map {
                    TilesAdapter.Tile(it.id ?: 0, it.image ?: "", it.title ?: "", "") {
                        onClick?.invoke(it)
                    }
                })
                binding.sectionMediaRecyclerView.adapter = adapter
            }

            override fun onSaveItemViewHolderState(state: SparseArray<Parcelable>) {
                binding.sectionMediaRecyclerView.saveHierarchyState(state)
            }

            override fun onRestoreItemViewHolderState(state: SparseArray<Parcelable>) {
                binding.sectionMediaRecyclerView.restoreHierarchyState(state)
            }
        }
    }

    sealed class State {
        data class Success(val items: List<Playlist>) : State()
        data class Error(val exception: Throwable) : State()
        data class Loading(val progress: Int) : State()
    }
}