package com.example.music.ui.main

import android.accounts.AccountManager
import android.accounts.OnAccountsUpdateListener
import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import androidx.activity.viewModels
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavGraph
import androidx.navigation.NavOptions
import androidx.navigation.fragment.DialogFragmentNavigator
import androidx.navigation.fragment.FragmentNavigator
import by.kirich1409.viewbindingdelegate.viewBinding
import co.velloso.android.statefulnavigation.StatefulNavController
import co.velloso.android.statefulnavigation.activity.NavHostActivity
import co.velloso.android.statefulnavigation.extensions.setUpBottomSheetNavigation
import co.velloso.android.statefulnavigation.extensions.setUpNavigation
import co.velloso.android.statefulnavigation.navigators.BottomSheetNavigator
import co.velloso.android.statefulnavigation.navigators.StatefulFragmentNavigator
import com.example.music.BuildConfig
import com.example.music.R
import com.example.music.data.PreferenceStorage
import com.example.music.data.PreferenceStorage.Companion.updateForTheme
import com.example.music.databinding.MainBinding
import com.example.music.service.triggerRefresh
import com.google.android.material.navigation.NavigationBarView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class MainActivity : NavHostActivity(R.layout.main),
    NavigationBarView.OnItemReselectedListener {

    override var graphId = R.navigation.navigation_main

    private val accountsUpdateListener = OnAccountsUpdateListener { accounts ->
        val account = accounts.find { it.type == BuildConfig.ACCOUNT_TYPE }
        if (account == null) {
            navController.navigate(R.id.loginFragment)
            finish()
        } else account.triggerRefresh(true)
    }

    private val viewModel: MainViewModel by viewModels()

    val binding: MainBinding by viewBinding(MainBinding::bind)

    override fun onCreate(savedInstanceState: Bundle?) {
        AccountManager.get(this).addOnAccountsUpdatedListener(
            accountsUpdateListener, null,
            savedInstanceState == null
        )
        setTheme(R.style.AppTheme) // Change the launch screen to the app theme
        super.onCreate(savedInstanceState)

        // Use SYSTEM as default (see theme StateFlow default) and subscribe to changes of theme
        updateForTheme(viewModel.theme.value)
        viewModel.theme.flowWithLifecycle(lifecycle)
            .onEach { item -> updateForTheme(item) }
            .launchIn(lifecycleScope)

        // Set up bottom sheet
        binding.playerView.visibility = GONE
        binding.playerView.setUpBottomSheetNavigation(navController, R.id.playerBottomSheet)

        // Set up navigation bar
        val navOptions = NavOptions.Builder()
            .setEnterAnim(R.anim.nav_default_enter_anim)
            .setExitAnim(R.anim.nav_default_exit_anim)
            .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
            .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
            .build()
        binding.bottomNavigationView.setUpNavigation(navController, navOptions, this)
    }

    override fun onDestroy() {
        super.onDestroy()
        AccountManager.get(this).removeOnAccountsUpdatedListener(accountsUpdateListener)
    }

    override fun onCreateNavController() = StatefulNavController(this).apply {
        navigatorProvider.addNavigator(
            FragmentNavigator(
                this@MainActivity,
                supportFragmentManager,
                R.id.container
            )
        )
        navigatorProvider.addNavigator(
            StatefulFragmentNavigator(
                this@MainActivity,
                supportFragmentManager,
                R.id.container
            )
        )
        navigatorProvider.addNavigator(
            DialogFragmentNavigator(
                this@MainActivity,
                supportFragmentManager
            )
        )
        navigatorProvider.addNavigator(BottomSheetNavigator(this@MainActivity))
    }

    override fun onNavigationItemReselected(item: MenuItem) {
        // navigate to the root of current selected navigation
        navController.currentDestination?.let {
            if (it is NavGraph) false // we are already on root
            else navController.popBackStack(it.parent!!.startDestinationId, false)
        }
    }

    override fun onSupportNavigateUp() = navController.navigateUp()

    override fun onBackPressed() {
        when {
            navController.navigateUp() -> {
            }
            // Ensure the user always leaves from startDestination
            navController.currentDestination?.parent?.id != navController.graph.startDestinationId -> {
                // clear the back stack otherwise it goes into an infinite loop
                navController.popBackStack(navController.currentDestination!!.id, true)
                navController.navigate(navController.graph.startDestinationId)
            }
            else -> finish()
        }
    }
}