package com.example.music.ui.common

import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import co.velloso.android.statefulrecyclerview.StatefulViewHolder
import com.example.music.R
import com.example.music.common.setImageOptimized
import com.example.music.databinding.TileBinding

class TilesAdapter : ListAdapter<TilesAdapter.Tile, TilesAdapter.ItemViewHolder>(Tile.Differ) {

    data class Tile(
        val id: Long,
        val image: String,
        val title: String,
        val subtitle: String,
        val isSquare: Boolean = true,
        val listener: (() -> Unit)? = null
    ) {
        object Differ : DiffUtil.ItemCallback<Tile>() {
            override fun areItemsTheSame(oldItem: Tile, newItem: Tile) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Tile, newItem: Tile) =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemViewHolder(parent)

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(parent: ViewGroup) : StatefulViewHolder(parent, R.layout.tile) {

        override fun bind() {
            val tile = getItem(bindingAdapterPosition)
            val binding = TileBinding.bind(itemView)
            binding.tileImageView.setImageOptimized(tile.image)
            (binding.tileCardView.layoutParams as ConstraintLayout.LayoutParams)
                .dimensionRatio = if (tile.isSquare) "1:1" else "16:9"
            binding.tileTitleTextView.text = tile.title
            binding.tileSubtitleTextView.text = tile.subtitle
            binding.root.setOnClickListener {
                tile.listener?.invoke()
            }
        }
    }
}