package com.example.music.ui.account

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.music.data.*
import com.example.music.di.ApplicationScope
import com.example.music.domain.GetThemeUseCase
import com.example.music.domain.UseCase.Result.Companion.successOr
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(
    @ApplicationScope externalScope: CoroutineScope,
    private val database: Database,
    private val preferenceStorage: PreferenceStorage,
) : ViewModel() {

    fun updateTheme(theme: PreferenceStorage.Theme) {
        viewModelScope.launch {
            preferenceStorage.selectTheme(theme.storageKey)
        }
    }

    val users: Flow<List<User>> by lazy {
        database.user().listAsFlow()
    }
}