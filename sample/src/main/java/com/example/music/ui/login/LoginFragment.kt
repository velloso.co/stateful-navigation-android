package com.example.music.ui.login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.music.R
import com.example.music.databinding.LoginBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment(R.layout.login) {

    interface LoginFragmentListener {
        fun onSignUp() {}
        fun onSignIn() {}
    }

    private val listener: LoginFragmentListener by lazy {
        activity as LoginFragmentListener
    }

    private val binding: LoginBinding by viewBinding(LoginBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.signUpButton.setOnClickListener {
            listener.onSignUp()
        }
        binding.signInButton.setOnClickListener {
            listener.onSignIn()
        }
    }
}
