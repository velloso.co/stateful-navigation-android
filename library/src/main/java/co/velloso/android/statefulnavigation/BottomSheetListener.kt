package co.velloso.android.statefulnavigation

interface BottomSheetListener {
    fun onExpanded() {}
    fun onHalfExpanded() {}
    fun onSettling() {}
    fun onCollapsed() {}
    fun onHidden() {}
    fun onDragging() {}
    fun onSlide(progress: Float) {}
}