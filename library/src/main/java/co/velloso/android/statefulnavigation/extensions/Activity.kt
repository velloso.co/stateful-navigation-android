package co.velloso.android.statefulnavigation.extensions

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.navigation.NavHost


fun Activity.findNavController() = (this as? NavHost)?.navController

fun Activity.activityNavController() = lazy {
    (this as NavHost).navController
}