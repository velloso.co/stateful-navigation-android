package co.velloso.android.statefulnavigation.navigators

import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.fragment.R
import java.util.*
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set


/**
 * A stateful version of Android Arch Navigation FragmentNavigator.java.
 */
@Navigator.Name(StatefulFragmentNavigator.NAME)
class StatefulFragmentNavigator constructor(
        private val context: Context,
        private val fragmentManager: FragmentManager,
        @IdRes private val containerId: Int
) : Navigator<StatefulFragmentNavigator.Destination>() {

    override fun navigate(
            destination: Destination,
            args: Bundle?,
            navOptions: NavOptions?,
            navigatorExtras: Navigator.Extras?
    ): NavDestination? {

        if (fragmentManager.isStateSaved) {
            Log.i("TAG", "Ignoring navigate() call: FragmentManager has already" + " saved its state")
            return null
        }

        // The trick is to try to find the same fragment on the back stack before creating a new
        // one, then move it to the last item of back stack instead of adding it
        val stackFrag = fragmentManager.findFragmentByTag(createFragmentTag(destination.parent?.id, destination.id))
        val frag = if (stackFrag != null) stackFrag
        else {
            val className = if (destination.className[0] != '.') destination.className
            else context.packageName + destination.className
            fragmentManager.fragmentFactory.instantiate(context.classLoader, className).apply {
                arguments = args
            }
        }

        if (frag.arguments != null) frag.requireArguments().putAll(args ?: Bundle())
        else frag.arguments = args

        val ft = fragmentManager.beginTransaction()

        var enterAnim = navOptions?.enterAnim ?: -1
        var exitAnim = navOptions?.exitAnim ?: -1
        var popEnterAnim = navOptions?.popEnterAnim ?: -1
        var popExitAnim = navOptions?.popExitAnim ?: -1
        if (enterAnim != -1 || exitAnim != -1 || popEnterAnim != -1 || popExitAnim != -1) {
            enterAnim = if (enterAnim != -1) enterAnim else 0
            exitAnim = if (exitAnim != -1) exitAnim else 0
            popEnterAnim = if (popEnterAnim != -1) popEnterAnim else 0
            popExitAnim = if (popExitAnim != -1) popExitAnim else 0
            ft.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim)
        }

        ft.replace(containerId, frag, createFragmentTag(destination.parent?.id, destination.id))
        // very important. this fixes a very problematic bug that makes the state of the
        // NavHostFragment vanish, including the state of the NavController
        //ft.disallowAddToBackStack()
        ft.setPrimaryNavigationFragment(frag)
        ft.addToBackStack(createFragmentTag(destination.parent?.id, destination.id))
        if (navigatorExtras is Extras) {
            for (sharedElement in navigatorExtras.getSharedElements().entries) {
                ft.addSharedElement(sharedElement.key, sharedElement.value)
            }
        }
        ft.commit()

        return destination
    }

    override fun popBackStack(): Boolean {

        fragmentManager.primaryNavigationFragment?.let {
            fragmentManager.beginTransaction()
                    .remove(it)
                    .commit()
            return true
        }

        return false
    }

    override fun createDestination() = Destination(this)

    private fun createFragmentTag(parentId: Int?, destinationId: Int) = "${parentId
            ?: 0}:$destinationId"

    class Destination(navigator: Navigator<out NavDestination>) : NavDestination(navigator) {

        var className: String = ""
            get() = checkDiff(field, "") { "Error inflating XML. Attribute 'className' must be non-blank." }

        override fun onInflate(context: Context, attrs: AttributeSet) {
            super.onInflate(context, attrs)
            context.resources.useAttributes(attrs, R.styleable.FragmentNavigator) {
                className = it.getString(R.styleable.FragmentNavigator_android_name) ?: ""
            }
        }

        private inline fun Resources.useAttributes(
                attrs: AttributeSet,
                typedArrayIds: IntArray,
                block: (TypedArray) -> Unit
        ) {
            val typedArray = obtainAttributes(attrs, typedArrayIds)
            block(typedArray).also { typedArray.recycle() }
        }

        private fun <T : Any> checkDiff(value: T?, secondValue: T?, lazyMessage: () -> Any): T {
            if (value == null || value == secondValue) throw java.lang.RuntimeException(lazyMessage.toString())
            return value
        }
    }

    /**
     * Extras that can be passed to FragmentNavigator to enable Fragment specific behavior
     */
    class Extras internal constructor(sharedElements: Map<View, String>) : Navigator.Extras {
        private val mSharedElements = LinkedHashMap<View, String>()

        /**
         * Gets the map of shared elements associated with these Extras. The returned map
         * is an [unmodifiable][Collections.unmodifiableMap] copy of the underlying
         * map and should be treated as immutable.
         */
        fun getSharedElements(): Map<View, String> {
            return Collections.unmodifiableMap(mSharedElements)
        }

        init {
            mSharedElements.putAll(sharedElements)
        }

        /**
         * Builder for constructing new [Extras] instances. The resulting instances are
         * immutable.
         */
        class Builder {
            private val mSharedElements = LinkedHashMap<View, String>()

            /**
             * Adds multiple shared elements for mapping Views in the current Fragment to
             * transitionNames in the Fragment being navigated to.
             *
             * @param sharedElements Shared element pairs to add
             * @return this [Builder]
             */
            fun addSharedElements(sharedElements: Map<View, String>): Builder {
                for ((view, name) in sharedElements) {
                    addSharedElement(view, name)
                }
                return this
            }

            /**
             * Maps the given View in the current Fragment to the given transition name in the
             * Fragment being navigated to.
             *
             * @param sharedElement A View in the current Fragment to match with a View in the
             * Fragment being navigated to.
             * @param name The transitionName of the View in the Fragment being navigated to that
             * should be matched to the shared element.
             * @return this [Builder]
             * @see FragmentTransaction.addSharedElement
             */
            fun addSharedElement(sharedElement: View, name: String): Builder {
                mSharedElements[sharedElement] = name
                return this
            }

            /**
             * Constructs the final [Extras] instance.
             *
             * @return An immutable [Extras] instance.
             */
            fun build(): Extras {
                return Extras(mSharedElements)
            }
        }
    }

    companion object {
        const val NAME = "stateful-fragment"
    }
}