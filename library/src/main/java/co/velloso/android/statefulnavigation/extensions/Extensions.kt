package co.velloso.android.statefulnavigation.extensions

import android.os.Build
import android.view.View
import androidx.annotation.IdRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.iterator
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import co.velloso.android.statefulnavigation.BottomSheetListener
import co.velloso.android.statefulnavigation.R
import co.velloso.android.statefulnavigation.navigators.BottomSheetNavigator
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.navigation.NavigationView


// Convenience extension to set up the drawer navigation
fun NavigationView.setUpNavigation(navController: NavController,
                                   activity: AppCompatActivity? = null,
                                   drawerLayout: DrawerLayout? = null,
                                   toolbar: Toolbar? = null) {
    activity?.let {
        // Initialize action bar
        activity.setSupportActionBar(toolbar)
        activity.supportActionBar?.setDisplayShowTitleEnabled(false)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Set up toggle drawer button
        drawerLayout?.let { drawerLayout ->
            val toggle = ActionBarDrawerToggle(activity, drawerLayout, toolbar,
                    R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close
            )
            val drawerToggle = DrawerArrowDrawable(activity)
            toggle.setHomeAsUpIndicator(drawerToggle)
            drawerLayout?.addDrawerListener(toggle)
            toggle.syncState()
        }

        // Adjust the insets
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            setOnApplyWindowInsetsListener { v, insets ->
                v.setPadding(0, insets.systemWindowInsetTop, 0, 0)
                insets.consumeSystemWindowInsets()
            }
        }
    }

    drawerLayout ?: return

    (0 until headerCount).forEach {
        getHeaderView(it).setOnClickListener { v ->
            drawerLayout.onItemClickListener(v.id, navController)
        }
    }
    setNavigationItemSelectedListener {
        drawerLayout.onItemClickListener(it.itemId, navController)
    }
    navController.addOnDestinationChangedListener { _, destination, _ ->
        menu.iterator().forEach {
            it.isChecked = it.itemId == destination.id || it.itemId == destination.parent?.id
        }
    }
}

private fun DrawerLayout.onItemClickListener(@IdRes itemId: Int, navController: NavController): Boolean {
    // If drawer is closed, call onFragmentSelected immediately
    if (!isDrawerOpen(GravityCompat.START)) {
        navController.navigate(itemId)
        return true
    }

    // If drawer layout is open, call the onFragmentSelected only after close animation of
    // drawer finishes to avoid UI animation lags
    addDrawerListener(object : DrawerLayout.DrawerListener {
        override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
        override fun onDrawerOpened(drawerView: View) {}
        override fun onDrawerStateChanged(newState: Int) {}
        override fun onDrawerClosed(drawerView: View) {

            // Navigate
            navController.navigate(itemId)

            // Remove this listener
            removeDrawerListener(this)
        }
    })

    closeDrawer(GravityCompat.START)
    return true
}

/**
 * Sync the state of a NavController with the state of a View's BottomSheetBehavior.
 *
 * This extension sets the bottom sheet callback. Do not reset the callback otherwise
 * the state will not be in sync with NavController. If it's necessary to
 * listen to the state do so by providing the [listener] param.
 *
 * @param navController navigation controller that this bottom sheet must be synced with
 * @param destinationId bottom sheet destination ID on the navigation graph of [navController]
 * @param listener a listener to propagate the change of  BottomSheetBehavior's state.
 *                 If not supplied, it tries to use the view as BottomSheetListener
 */
fun View.setUpBottomSheetNavigation(
        navController: NavController,
        @IdRes destinationId: Int,
        listener: BottomSheetListener? = this as? BottomSheetListener
) {
    BottomSheetBehavior.from(this).setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {

        override fun onSlide(view: View, progress: Float) {
            listener?.onSlide(progress)
        }

        override fun onStateChanged(view: View, state: Int) {
            when (state) {
                BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    listener?.onHalfExpanded()
                }
                BottomSheetBehavior.STATE_HIDDEN -> {
                    listener?.onHidden()
                }
                BottomSheetBehavior.STATE_SETTLING -> {
                    listener?.onSettling()
                }
                BottomSheetBehavior.STATE_EXPANDED -> {
                    listener?.onExpanded()
                    // Ensure every expand event is added to the back stack
                    if (navController.currentDestination?.navigatorName != BottomSheetNavigator.NAME) {
                        navController.navigate(destinationId)
                    }
                }
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    listener?.onCollapsed()
                    // Ensure every collapse event is removed from the back stack
                    if (navController.currentDestination?.navigatorName == BottomSheetNavigator.NAME) {
                        navController.popBackStack()
                    }
                }
                BottomSheetBehavior.STATE_DRAGGING -> {
                    listener?.onDragging()
                }
            }
        }
    })
}