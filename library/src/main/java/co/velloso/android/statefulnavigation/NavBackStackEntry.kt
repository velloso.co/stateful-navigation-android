package co.velloso.android.statefulnavigation

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination

/**
 * Representation of an entry in the back stack of a [NavController].
 */
class NavBackStackEntry(

        /**
         * The destination associated with this entry
         * The destination that is currently visible to users
         */
        val destination: NavDestination,
        /**
         * The arguments used for this entry
         * @return The arguments used when this entry was created
         */
        val arguments: Bundle?
) {
    override fun equals(other: Any?): Boolean = if (other is NavBackStackEntry) {
        other.destination.id == destination.id && other.destination.parent?.id == destination.parent?.id
    } else false

    override fun hashCode(): Int {
        var result = destination.hashCode()
        result = 31 * result + (arguments?.hashCode() ?: 0)
        return result
    }
}