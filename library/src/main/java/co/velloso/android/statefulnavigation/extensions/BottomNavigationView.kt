package co.velloso.android.statefulnavigation.extensions

import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView

fun BottomNavigationView.setUpNavigation(
        navController: NavController,
        navOptions: NavOptions? = null,
        onReselect: NavigationBarView.OnItemReselectedListener? = null
) {
    setOnItemSelectedListener {
        navController.navigate(it.itemId, null, navOptions, null)
        true
    }
    navController.addOnDestinationChangedListener { _, destination, _ ->
        menu.findItem(destination.parent!!.id)?.isChecked = true
    }
    onReselect?.let {
        setOnItemReselectedListener(it)
    }
}