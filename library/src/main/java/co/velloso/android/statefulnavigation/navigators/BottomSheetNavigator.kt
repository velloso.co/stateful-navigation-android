package co.velloso.android.statefulnavigation.navigators

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import androidx.annotation.LayoutRes
import androidx.core.os.bundleOf
import androidx.core.util.putAll
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import co.velloso.android.statefulnavigation.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import java.util.*

/**
 * Allows to navigate to a [View] that need to be added.
 *
 * Usage: add some dialog element in your navigation graph
 * ```
 *     <bottom-sheet
 *         android:id="@+id/my_bottom_sheet"
 *         tools:layout="@layout/my_bottom_sheet" />
 * ```
 */
@Navigator.Name(BottomSheetNavigator.NAME)
open class BottomSheetNavigator(
        /**
         * Container where the bottom sheet should be looked for
         */
        private val activity: Activity
) : Navigator<BottomSheetNavigator.Destination>() {

    private val backStack: Deque<Destination> = ArrayDeque()

    private val viewStates: SparseArray<Parcelable> = SparseArray()

    private var listeners: MutableList<OnNavigationChangedListener> = mutableListOf()

    interface OnNavigationChangedListener {
        fun onNavigationAdded(destination: NavDestination, arguments: Bundle?) {}
        fun onNavigationPop() {}
    }

    fun addOnNavigationAddedListener(listener: OnNavigationChangedListener) {
        listeners.add(listener)
    }

    override fun navigate(
            destination: Destination, args: Bundle?,
            navOptions: NavOptions?, navigatorExtras: Navigator.Extras?
    ): NavDestination? {

        val bottomSheetView = activity.findViewById<View>(destination.viewId) ?: return null
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView)
        bottomSheetBehavior.state = STATE_EXPANDED

        backStack.addLast(destination)
        listeners.forEach { it.onNavigationAdded(destination, args) }
        return destination
    }

    override fun popBackStack(): Boolean {

        val bottomSheetView = activity.findViewById<View>(
                backStack.lastOrNull()?.viewId
                        ?: return false
        )
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView)
        bottomSheetBehavior.state = STATE_COLLAPSED

        backStack.removeLast()
        listeners.forEach { it.onNavigationPop() }
        return true
    }

    override fun createDestination(): Destination = Destination(this)

    override fun onSaveState(): Bundle? {
        val bundle = bundleOf(
                KEY_BACK_STACK_IDS to backStack.map { it.id },
                KEY_BACK_STACK_VIEWS to backStack.map { it.viewId }
        )
        bundle.putSparseParcelableArray(KEY_BACK_STACK_VIEWS_STATE, viewStates)
        return bundle
    }

    override fun onRestoreState(savedState: Bundle) {
        // restore back stack
        val ids = savedState.getIntegerArrayList(KEY_BACK_STACK_IDS)
        val layouts = savedState.getIntegerArrayList(KEY_BACK_STACK_VIEWS)
        if (ids != null && layouts != null) {
            backStack.clear()
            Pair(ids, layouts).iterator().forEach {
                val destination = Destination(this)
                destination.id = it.first
                destination.viewId = it.second
                backStack.addLast(destination)
            }
        }

        // restore views state
        viewStates.clear()
        savedState.getSparseParcelableArray<Parcelable>(KEY_BACK_STACK_VIEWS_STATE)?.let {
            viewStates.putAll(it)
        }
    }

    class Destination(navigator: BottomSheetNavigator) : NavDestination(navigator) {

        @LayoutRes
        var viewId: Int = 0
            get() = checkDiff(field, 0) { "Error inflating XML. Attribute 'layout' must be non-zero" }

        override fun onInflate(context: Context, attrs: AttributeSet) {
            super.onInflate(context, attrs)
            context.resources.useAttributes(attrs, R.styleable.BottomSheetNavigator) {
                viewId = it.getResourceId(R.styleable.BottomSheetNavigator_view, 0)
            }
        }

        private inline fun <R> Resources.useAttributes(
                attrs: AttributeSet,
                typedArrayIds: IntArray,
                block: (TypedArray) -> R
        ): R {
            val typedArray = obtainAttributes(attrs, typedArrayIds)
            return block(typedArray).also {
                typedArray.recycle()
            }
        }

        private fun <T : Any> checkDiff(value: T?, secondValue: T?, lazyMessage: () -> Any): T {
            if (value == null || value == secondValue) throw java.lang.RuntimeException(lazyMessage.toString())
            return value
        }
    }

    class Extras(sharedElements: Map<String, View>) : Navigator.Extras {
        private val mSharedElements = LinkedHashMap<String, View>()

        val sharedElements: Map<String, View>
            get() = Collections.unmodifiableMap(mSharedElements)

        init {
            mSharedElements.putAll(sharedElements)
        }

        /**
         * Builder for constructing new [Extras] instances. The resulting instances are
         * immutable.
         */
        class Builder {
            private val mSharedElements = LinkedHashMap<String, View>()

            fun addSharedElements(sharedElements: Map<String, View>): Builder {
                for ((name, view) in sharedElements) {
                    addSharedElement(name, view)
                }
                return this
            }

            fun addSharedElement(name: String, sharedElement: View): Builder {
                mSharedElements[name] = sharedElement
                return this
            }

            fun build(): Extras {
                return Extras(mSharedElements)
            }
        }
    }

    companion object {
        const val NAME = "bottom-sheet"
        private const val KEY_BACK_STACK_IDS = "co.velloso.android:navigation:back_stack_ids"
        private const val KEY_BACK_STACK_VIEWS = "co.velloso.android:navigation:back_stack_layouts"
        private const val KEY_BACK_STACK_VIEWS_STATE = "co.velloso.android:navigation:back_stack_views_state"

        const val EXTRAS_SHARED_ELEMENT_CONTAINER =
                " co.velloso.android.toolbox.navigators.ViewNavigator.EXTRAS_SHARED_ELEMENT_CONTAINER"

        fun extrasOf(vararg sharedElements: Pair<String, View>) =
                Extras.Builder().apply {
                    sharedElements.forEach { (name, view) ->
                        addSharedElement(name, view)
                    }
                }.build()

        /**
         * Returns a iterator for a pair of iterables in such a way that it only goes next if both
         * has next.
         */
        private fun <A, B> Pair<Iterable<A>, Iterable<B>>.iterator(): Iterator<Pair<A, B>> {
            val ia = first.iterator()
            val ib = second.iterator()
            return object : Iterator<Pair<A, B>> {
                override fun next(): Pair<A, B> = Pair(ia.next(), ib.next())
                override fun hasNext(): Boolean = (ia.hasNext() && ib.hasNext())
            }
        }
    }
}


