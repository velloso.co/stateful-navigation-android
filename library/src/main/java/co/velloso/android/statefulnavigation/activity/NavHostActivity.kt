package co.velloso.android.statefulnavigation.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.annotation.NavigationRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavHost

abstract class NavHostActivity : AppCompatActivity, NavHost {

    @NavigationRes open var graphId: Int = 0

    private lateinit var navControllerInternal: NavController

    override val navController: NavController
        get() = run {
            try {
                return navControllerInternal
            } catch (e: UninitializedPropertyAccessException) {
                throw UninitializedPropertyAccessException("NavController is not available before onCreate()")
            }
        }

    constructor() : super()

    constructor(@LayoutRes contentLayoutId: Int) : super(contentLayoutId)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navControllerInternal = onCreateNavController()

        var navState: Bundle? = null
        if (savedInstanceState != null) {
            navState = savedInstanceState.getBundle(KEY_NAV_CONTROLLER_STATE)
        }
        if (navState != null) {
            // Navigation controller state overrides arguments
            navControllerInternal.restoreState(navState)
        }
        if (graphId != 0) {
            // Set by overriding the graphId field
            navControllerInternal.setGraph(graphId)
        }
    }

    open fun onCreateNavController() = NavController(this)

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val navState = navControllerInternal.saveState()
        if (navState != null) {
            outState.putBundle(KEY_NAV_CONTROLLER_STATE, navState)
        }
    }

    companion object {
        private const val KEY_NAV_CONTROLLER_STATE = "co.velloso.android.navigation:activity:navControllerState"
    }
}