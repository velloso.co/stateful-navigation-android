package co.velloso.android.statefulnavigation.extensions

import androidx.fragment.app.Fragment
import androidx.navigation.NavHost


fun Fragment.findNavController() = (this as? NavHost)?.navController
        ?: (activity as? NavHost)?.navController

fun Fragment.navController() = lazy {
    this.findNavController()!!
}

fun Fragment.activityNavController() = lazy {
    (activity as NavHost).navController
}