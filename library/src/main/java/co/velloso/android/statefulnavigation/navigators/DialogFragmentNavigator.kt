package co.velloso.android.statefulnavigation.navigators

import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.os.Bundle
import android.util.AttributeSet
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import co.velloso.android.statefulnavigation.R
import java.util.*

/**
 * Allows to navigate to [DialogFragment] instances.
 *
 * Usage: add some dialog element in your navigation graph
 * ```
 *     <dialog-fragment android:id="@+id/my_dialog"
 *               android:name="com.exemple.MyDialogFragment"/>
 * ```
 * Add the DialogFragmentNavigator to your
 * [NavigatorProvider][androidx.navigation.NavigatorProvider]
 */
@Navigator.Name(DialogFragmentNavigator.NAME)
class DialogFragmentNavigator(
        private val context: Context,
        private val fragmentManager: FragmentManager
) : Navigator<DialogFragmentNavigator.Destination>() {

    private var lastBackStackEntry: FragmentManager.BackStackEntry? = null
    private val backStack: Deque<Int> = ArrayDeque()

    override fun navigate(
            destination: Destination, args: Bundle?,
            navOptions: NavOptions?, navigatorExtras: Extras?
    ): NavDestination? {

        val dialogFragment = fragmentManager.fragmentFactory
                .instantiate(context.classLoader, destination.className!!) as DialogFragment
        dialogFragment.arguments = args

        val tr = fragmentManager.beginTransaction().addToBackStack(destination.id.toString())
        dialogFragment.show(tr, destination.id.toString())

        backStack.addLast(destination.id)
        return destination
    }

    override fun createDestination(): Destination = Destination(this)

    override fun popBackStack(): Boolean {
        val dialogFragment = fragmentManager
                .findFragmentByTag(backStack.lastOrNull()?.toString()) as? DialogFragment
                ?: return false
        dialogFragment.dismiss()

        backStack.removeLast()
        return true
    }

    override fun onSaveState(): Bundle? {
        return bundleOf(KEY_BACK_STACK_ID to backStack.toIntArray())
    }

    override fun onRestoreState(savedState: Bundle) {
        savedState.getIntArray(KEY_BACK_STACK_ID)?.let {
            backStack.clear()
            for (id in it) {
                backStack.addLast(id)
            }
        }
        backStack.lastOrNull()?.let {
            lastBackStackEntry = fragmentManager.findLastBackStackEntry { it.name == it.toString() }
        }
    }

    class Destination(navigator: DialogFragmentNavigator) : NavDestination(navigator) {

        var className: String? = null
            get() = checkNotNull(field) { "Error inflating XML. 'dialog-fragment' attribute 'name' was not set" }

        override fun onInflate(context: Context, attrs: AttributeSet) {
            super.onInflate(context, attrs)
            context.resources.useAttributes(attrs, R.styleable.FragmentNavigator) {
                className = it.getString(R.styleable.FragmentNavigator_android_name)
            }
        }

        private inline fun <R> Resources.useAttributes(
                attrs: AttributeSet,
                typedArrayIds: IntArray,
                block: (TypedArray) -> R
        ): R {
            val typedArray = obtainAttributes(attrs, typedArrayIds)
            return block(typedArray).also { typedArray.recycle() }
        }
    }

    companion object {
        const val NAME = "dialog-fragment"

        private const val KEY_BACK_STACK_ID = "co.velloso.android:dialog_navigation_navigator:back_stack_ids"
    }

    private inline fun FragmentManager.findLastBackStackEntry(
            predicate: (FragmentManager.BackStackEntry) -> Boolean
    ): FragmentManager.BackStackEntry? {
        for (i in backStackEntryCount - 1 downTo 0) {
            val backStackEntry = getBackStackEntryAt(i)
            if (predicate(backStackEntry)) {
                return backStackEntry
            }
        }
        return null
    }
}