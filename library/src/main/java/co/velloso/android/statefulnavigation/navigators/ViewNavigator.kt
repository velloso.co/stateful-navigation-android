package co.velloso.android.statefulnavigation.navigators

import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.os.bundleOf
import androidx.core.util.putAll
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import co.velloso.android.statefulnavigation.R
import java.util.*

/**
 * Allows to navigate to a [View] that need to be added.
 *
 * Usage: add some view element in your navigation graph
 * ```
 *     <view android:id="@+id/my_view"
 *         android:layout="@layout/my_view"/>
 * ```
 * Then pass the container View where it should be added using the extras
 * EXTRAS_SHARED_ELEMENT_CONTAINER
 */
@Navigator.Name(ViewNavigator.NAME)
class ViewNavigator(
        private val inflater: LayoutInflater
) : Navigator<ViewNavigator.Destination>() {

    private val backStack: Deque<Destination> = ArrayDeque()

    private val viewStates: SparseArray<Parcelable> = SparseArray()

    private lateinit var container: ViewGroup

    override fun navigate(
            destination: Destination, args: Bundle?,
            navOptions: NavOptions?, navigatorExtras: Navigator.Extras?
    ): NavDestination? {
        if (navigatorExtras == null) return null

        container = (navigatorExtras as? Extras)?.sharedElements?.get(EXTRAS_SHARED_ELEMENT_CONTAINER) as? ViewGroup
                ?: throw RuntimeException("Shared element container is mandatory.")

        // remove the current view
        backStack.lastOrNull()?.let {
            val view: View = container.findViewById(it.id) ?: return@let
            view.saveHierarchyState(viewStates)
            container.removeView(view)
        }

        // add next view
        val view = inflater.inflate(destination.layoutResId, container, false)
        view.id = destination.id
        view.restoreHierarchyState(viewStates)
        container.addView(view)

        // Check if it's a new destination on the stack or if we just want to repopulate the
        // container (like in a orientation change)
        return if (destination.id != backStack.lastOrNull()?.id) {
            backStack.addLast(destination)
            destination
        } else null
    }

    override fun popBackStack(): Boolean {

        if (backStack.isEmpty()) return true

        // remove the current view
        val currentDestination = backStack.lastOrNull() ?: return true
        container.removeView(container.findViewById(currentDestination.id))
        backStack.removeLast()

        // add the previous view
        val previousDestination = backStack.lastOrNull() ?: return true
        val view = inflater.inflate(previousDestination.layoutResId, container, false)
        view.id = previousDestination.id
        view.restoreHierarchyState(viewStates)
        container.addView(view)

        return true
    }

    override fun createDestination(): Destination = Destination(this)

    override fun onSaveState(): Bundle? {
        val bundle = bundleOf(
                KEY_BACK_STACK_IDS to backStack.map { it.id },
                KEY_BACK_STACK_LAYOUTS to backStack.map { it.layoutResId }
        )
        bundle.putSparseParcelableArray(KEY_BACK_STACK_VIEWS_STATE, viewStates)
        return bundle
    }

    override fun onRestoreState(savedState: Bundle) {
        // restore back stack
        val ids = savedState.getIntegerArrayList(KEY_BACK_STACK_IDS)
        val layouts = savedState.getIntegerArrayList(KEY_BACK_STACK_LAYOUTS)
        if (ids != null && layouts != null) {
            backStack.clear()
            Pair(ids, layouts).iterator().forEach {
                val destination = Destination(this)
                destination.id = it.first
                destination.layoutResId = it.second
                backStack.addLast(destination)
            }
        }

        // restore views state
        viewStates.clear()
        savedState.getSparseParcelableArray<Parcelable>(KEY_BACK_STACK_VIEWS_STATE)?.let {
            viewStates.putAll(it)
        }
    }

    class Destination(navigator: ViewNavigator) : NavDestination(navigator) {

        @LayoutRes
        var layoutResId: Int = 0
            get() = checkDiff(field, 0) { "Error inflating XML. Attribute 'layout' must be non-zero" }

        override fun onInflate(context: Context, attrs: AttributeSet) {
            super.onInflate(context, attrs)
            context.resources.useAttributes(attrs, R.styleable.ViewNavigator) {
                layoutResId = it.getResourceId(R.styleable.ViewNavigator_layout, 0)
            }
        }

        private inline fun <R> Resources.useAttributes(
                attrs: AttributeSet,
                typedArrayIds: IntArray,
                block: (TypedArray) -> R
        ): R {
            val typedArray = obtainAttributes(attrs, typedArrayIds)
            return block(typedArray).also {
                typedArray.recycle()
            }
        }

        private fun <T : Any> checkDiff(value: T?, secondValue: T?, lazyMessage: () -> Any): T {
            if (value == null || value == secondValue) throw RuntimeException(lazyMessage.toString())
            return value
        }
    }

    class Extras(sharedElements: Map<String, View>) : Navigator.Extras {
        private val mSharedElements = LinkedHashMap<String, View>()

        val sharedElements: Map<String, View>
            get() = Collections.unmodifiableMap(mSharedElements)

        init {
            mSharedElements.putAll(sharedElements)
        }

        /**
         * Builder for constructing new [Extras] instances. The resulting instances are
         * immutable.
         */
        class Builder {
            private val mSharedElements = LinkedHashMap<String, View>()

            fun addSharedElements(sharedElements: Map<String, View>): Builder {
                for ((name, view) in sharedElements) {
                    addSharedElement(name, view)
                }
                return this
            }

            fun addSharedElement(name: String, sharedElement: View): Builder {
                mSharedElements[name] = sharedElement
                return this
            }

            fun build(): Extras {
                return Extras(mSharedElements)
            }
        }
    }

    companion object {
        const val NAME = "view"
        private const val KEY_BACK_STACK_IDS = "co.velloso.android:navigation:back_stack_ids"
        private const val KEY_BACK_STACK_LAYOUTS = "co.velloso.android:navigation:back_stack_layouts"
        private const val KEY_BACK_STACK_VIEWS_STATE = "co.velloso.android:navigation:back_stack_views_state"

        const val EXTRAS_SHARED_ELEMENT_CONTAINER =
                " co.velloso.android.toolbox.navigators.ViewNavigator.EXTRAS_SHARED_ELEMENT_CONTAINER"

        fun extrasOf(vararg sharedElements: Pair<String, View>) =
                Extras.Builder().apply {
                    sharedElements.forEach { (name, view) ->
                        addSharedElement(name, view)
                    }
                }.build()


        /**
         * Returns a iterator for a pair of interables in such a way that it only goes next if both
         * has next.
         */
        private fun <A, B> Pair<Iterable<A>, Iterable<B>>.iterator(): Iterator<Pair<A, B>> {
            val ia = first.iterator()
            val ib = second.iterator()
            return object : Iterator<Pair<A, B>> {
                override fun next(): Pair<A, B> = Pair(ia.next(), ib.next())
                override fun hasNext(): Boolean = (ia.hasNext() && ib.hasNext())
            }
        }

        /**
         * Returns a iterator for a pair of interables in such a way that it only goes next if both
         * has next.
         */
        private fun <A, B, C> Triple<Iterable<A>, Iterable<B>, Iterable<C>>.iterator(): Iterator<Triple<A, B, C>> {
            val ia = first.iterator()
            val ib = second.iterator()
            val ic = third.iterator()
            return object : Iterator<Triple<A, B, C>> {
                override fun next(): Triple<A, B, C> = Triple(ia.next(), ib.next(), ic.next())
                override fun hasNext(): Boolean = (ia.hasNext() && ib.hasNext() && ic.hasNext())
            }
        }

        private fun <E> SparseArray<E>.iterator(): Iterator<E> {
            return object : Iterator<E> {
                var index = 0
                override fun next() = this@iterator.get(index)
                override fun hasNext(): Boolean = index < this@iterator.size()
            }
        }
    }
}


