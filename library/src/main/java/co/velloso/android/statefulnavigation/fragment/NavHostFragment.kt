package co.velloso.android.statefulnavigation.fragment

import android.os.Bundle
import android.view.KeyEvent.ACTION_UP
import android.view.KeyEvent.KEYCODE_BACK
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavHost


/**
 * This view is almost exactly like the NavHostFragment from the AndroidX library, except that
 * 1. It uses a bug free version of NavigationFragment
 * 2. It recursively look up on parent fragment for NavController on the static method
 * findNavController
 */
abstract class NavHostFragment : Fragment(), NavHost {

    // navigation graph Id
    abstract val graphId: Int

    private lateinit var navControllerInternal: NavController

    override val navController: NavController
        get() = run {
            try {
                return navControllerInternal
            } catch (e: UninitializedPropertyAccessException) {
                throw UninitializedPropertyAccessException("NavController is not available before onCreate()")
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navControllerInternal = createNavController()

        var navState: Bundle? = null
        if (savedInstanceState != null) {
            navState = savedInstanceState.getBundle(KEY_NAV_CONTROLLER_STATE)
        }
        if (navState != null) {
            // Navigation controller state overrides arguments
            navControllerInternal.restoreState(navState)
        }
        if (graphId != 0) {
            // Set by overriding the graphId field
            navControllerInternal.setGraph(graphId)
        }
    }

    override fun onStart() {
        super.onStart()
        view?.setOnKeyListener { _, keyCode, event ->

            // Dispatch the back press button to activity to handle
            if (keyCode == KEYCODE_BACK && event.action == ACTION_UP) {
                if (!navController.navigateUp()) activity?.onBackPressed()
                return@setOnKeyListener true
            }

            false
        }
    }

    open fun createNavController() = NavController(requireContext())

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val navState = navControllerInternal.saveState()
        if (navState != null) {
            outState.putBundle(KEY_NAV_CONTROLLER_STATE, navState)
        }
    }

    companion object {
        private const val KEY_NAV_CONTROLLER_STATE =
            "co.velloso.android.navigation:fragment:navControllerState"
    }
}
