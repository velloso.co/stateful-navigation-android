package co.velloso.android.statefulnavigation

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.annotation.IdRes
import androidx.navigation.*
import java.util.*

/**
 * Controller to navigate between [NavHostFragment][androidx.navigation.fragment.NavHostFragment]'s, to new activities, and BottomNavigationViews
 * only. Do not try to navigate to other types of fragments otherwise the currentDestination will be
 * inconsistent.
 *
 * To navigate internally in a view, use inside a Fragment {@link Fragment#findNavController()
 * Fragment.findNavController()}. This will find the default NavController implementation of
 * [NavHostFragment][androidx.navigation.fragment.NavHostFragment].
 *
 * It intends to be used as the main [NavController][androidx.navigation.NavController] by a
 * [NavHostActivity][co.velloso.android.statefulnavigation.activity.NavHostActivity].
 */
class StatefulNavController(ctx: Context) : NavController(ctx) {
    
    private val backStack = ArrayDeque<NavBackStackEntry>()

    private val listeners = mutableListOf<OnDestinationChangedListener>()

    override fun navigate(resId: Int) {
        navigate(resId, null)
    }

    override fun navigate(resId: Int, args: Bundle?) {
        navigate(resId, args, null)
    }

    override fun navigate(resId: Int, args: Bundle?, navOptions: NavOptions?) {
        navigate(resId, args, navOptions, null)
    }

    override fun navigate(
        resId: Int,
        args: Bundle?,
        navOptions: NavOptions?,
        navigatorExtras: Navigator.Extras?
    ) {

        val currentNode = backStack.lastOrNull()?.destination ?: graph

        // Case 1: Handle nav actions
        val navAction = currentNode.getAction(resId)
        if (navAction != null) {
            val actionDestination = currentNode.parent!!.findNode(navAction.destinationId)
                ?: throw IllegalStateException("Navigation destination node not found")
            val navActionArgs = navAction.defaultArguments
            val combinedArgs = Bundle()
            if (navActionArgs != null) combinedArgs.putAll(navActionArgs)
            if (args != null) combinedArgs.putAll(args)

            backStack.addLast(NavBackStackEntry(actionDestination, combinedArgs))
            navigate(actionDestination, combinedArgs, navOptions, navigatorExtras)
            return
        }

        // Look for the node that was requested to be navigate either on the currentNode or in
        // it's parents
        val destinationRequested = (currentNode as? NavGraph ?: currentNode.parent)?.let {
            it.findNode(resId) ?: it.findNodeInTree(resId)
        } ?: throw IllegalStateException("Could not found destination $resId in this graph.")

        // Case 2: Handle navigation change from graph to other graph, keeping the state
        // Move the parent and siblings to the end if destination is not parent.
        // We cannot move while iterating so first find them
        val toMove = backStack.filter {
            // move every one that has this destination as parent (children)
            (destinationRequested is NavGraph && it.destination.parent.areEquals(
                destinationRequested
            ))
        }
        // If we find one, that means we navigated to pre existed on backstack NavGraph, like
        // when the user presses the bottom bar navigation. So we should navigate to the last item
        // on the stack
        if (toMove.isNotEmpty()) {
            toMove.forEach { backStack.moveLast(it) }
            navigate(backStack.last().destination, args, navOptions, navigatorExtras)
            return
        }

        // Case 3: Navigate to a brand new destination that doesn't exist on the stack
        val dest = if (destinationRequested !is NavGraph) destinationRequested
        else destinationRequested.findNode(destinationRequested.startDestinationId)!!
        backStack.addLast(NavBackStackEntry(dest, args))
        navigate(dest, args, navOptions, navigatorExtras)
    }

    private fun navigate(
        node: NavDestination, args: Bundle?,
        navOptions: NavOptions?, navigatorExtras: Navigator.Extras?
    ) {

        val destination = navigatorProvider
            .getNavigator<Navigator<NavDestination>>(node.navigatorName)
            .navigate(node, args, navOptions, navigatorExtras)

        if (destination == null) backStack.removeLast()
        dispatchOnDestinationChanged()
    }

    private fun NavDestination?.areEquals(destination: NavDestination?): Boolean {
        return destination?.id == this?.id
                && destination?.parent?.id == this?.parent?.id
                && destination?.arguments == this?.arguments
    }

    private fun dumpBackStack() {
        val dump = "[Parent ID : Destination ID]" + backStack.joinToString(" > \n", "\n") {
            "[${
                it.destination.parent?.id?.getDisplayName()
                    ?.replace(context.applicationContext.packageName + ":id/", "")
            } : ${
                it.destination.id.getDisplayName()
                    .replace(context.applicationContext.packageName + ":id/", "")
            }]"
        }
        Log.d("BackStack Dump", dump)
    }

    private fun Int.getDisplayName(): String = try {
        context.resources.getResourceName(this)
    } catch (e: Resources.NotFoundException) {
        Integer.toString(this)
    }

    override fun navigateUp() = popBackStack()

    override fun popBackStack(): Boolean {
        return if (backStack.size == 1) false
        else popBackStack(
            currentDestination!!.id,
            true
        )  // Pop just the current destination off the stack
    }

    override fun popBackStack(@IdRes destinationId: Int, inclusive: Boolean): Boolean {
        return popBackStackInternal(destinationId, inclusive)
    }

    private fun popBackStackInternal(@IdRes destinationId: Int, inclusive: Boolean): Boolean {
        // Nothing to pop if the back stack is empty
        if (backStack.isEmpty()) return false

        val popOperations = ArrayList<Navigator<*>>()
        val iterator = backStack.descendingIterator()
        var foundDestination = false
        while (iterator.hasNext()) {
            val destination = iterator.next().destination
            if (inclusive || destination.id != destinationId) {
                popOperations.add(navigatorProvider.getNavigator(destination.navigatorName))
            }
            if (destination.id == destinationId) {
                foundDestination = true

                // If the destination is NavGraph, we need to also pop the start destination
                // because the NavGraphNavigator implementation does not do it. Do not add it
                // to pop operations because it is not in the back stack
                if (destination is NavGraph) {
                    val startDestination = destination.findNode(destination.startDestinationId)!!
                    navigatorProvider.getNavigator<Navigator<NavDestination>>(startDestination.navigatorName)
                        .popBackStack()
                }
                break
            }
        }
        if (!foundDestination) {
            // We were passed a destinationId that doesn't exist on our back stack.
            // Better to ignore the popBackStack than accidentally popping the entire stack
            Log.i(
                TAG,
                "Ignoring popBackStack to destination as it was not found on the current back stack"
            )
            return false
        }
        for (navigator in popOperations) {
            if (navigator.popBackStack()) {
                backStack.removeLast()
            } else {
                // The pop did not complete successfully, so stop immediately
                return false
            }
        }
        backStack.lastOrNull()?.let {
            navigate(it.destination, it.arguments, null, null)
        }
        return true
    }

    private fun dispatchOnDestinationChanged() {
        if (!backStack.isEmpty()) {
            val backStackEntry = backStack.peekLast()!!
            listeners.forEach {
                it.onDestinationChanged(this, backStackEntry.destination, backStackEntry.arguments)
            }
            dumpBackStack()
        }
    }

    override fun addOnDestinationChangedListener(listener: OnDestinationChangedListener) {
        listeners.add(listener)
    }

    override fun saveState(): Bundle? {
        var b = super.saveState()

        if (!backStack.isEmpty()) {
            b = b ?: Bundle()
            val backStackIds = IntArray(backStack.size)
            val backStackArgs = arrayOfNulls<Parcelable>(backStack.size)
            for ((index, backStackEntry) in backStack.withIndex()) {
                backStackIds[index] = backStackEntry.destination.id
                backStackArgs[index] = backStackEntry.arguments
            }
            b.putIntArray(KEY_BACK_STACK_IDS, backStackIds)
            b.putParcelableArray(KEY_BACK_STACK_ARGS, backStackArgs)
        }
        return b
    }

    private var backStackIdsToRestore: IntArray? = null
    private var backStackArgsToRestore: Array<Parcelable>? = null

    override val currentDestination: NavDestination?
        get() = backStack.lastOrNull()?.destination

    private var _graph: NavGraph? = null
    override var graph: NavGraph
        get() = _graph
            ?: throw IllegalStateException("You must call setGraph() before calling getGraph()")
        set(value) {
            super.graph = value
            _graph = value
        }

    override fun setGraph(graph: NavGraph, startDestinationArgs: Bundle?) {
        super.setGraph(graph, startDestinationArgs)
        _graph?.let {
            // Pop everything from the old graph off the back stack
            popBackStackInternal(it.id, true)
        }
        _graph = graph
        onGraphCreated(startDestinationArgs)
    }

    private fun onGraphCreated(startDestinationArgs: Bundle?) {
        navigate(graph.startDestinationId)
        backStackIdsToRestore?.let {
            backStack.clear()
            for ((index, id) in it.withIndex()) {
                val node = graph.findNodeInTree(id)
                    ?: throw RuntimeException("Could not find node with ID $id")
                val args = backStackArgsToRestore!!.getOrNull(index) as? Bundle?
                backStack.addLast(NavBackStackEntry(node, args))
            }
            backStack.lastOrNull()?.let { lastEntry ->
                navigate(lastEntry.destination.id, lastEntry.arguments, null, null)
            }
        }
        if (backStack.isEmpty()) {
            _graph?.let {
                val destination =
                    navigatorProvider.getNavigator<Navigator<NavDestination>>(it.navigatorName)
                        .navigate(it, startDestinationArgs, null, null)
                if (destination != null) backStack.addLast(
                    NavBackStackEntry(
                        destination,
                        startDestinationArgs
                    )
                )
            }
        }

    }

    override fun restoreState(navState: Bundle?) {
        super.restoreState(navState)

        backStackIdsToRestore = navState?.getIntArray(KEY_BACK_STACK_IDS) ?: return
        backStackArgsToRestore = navState.getParcelableArray(KEY_BACK_STACK_ARGS) ?: return
    }

    private fun NavGraph.findNodeInTree(@IdRes destId: Int): NavDestination? {
        forEach {
            if (it.id == destId) return findNode(destId)
            else if (it is NavGraph) {
                val dest = it.findNodeInTree(destId)
                if (dest != null) return dest
            }
        }
        return null
    }

    private fun <T> ArrayDeque<T>.moveLast(item: T) {
        remove(item)
        addLast(item)
    }

    companion object {
        const val TAG = "NavHostFragmentNavContr"
        private const val KEY_BACK_STACK_IDS =
            "co.velloso.android:nav_fragment_navigation_controller:back_stack_ids"
        private const val KEY_BACK_STACK_ARGS =
            "co.velloso.android:nav_fragment_navigation_controller:back_stack_args"
    }
}